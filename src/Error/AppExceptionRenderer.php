<?php

namespace App\Error;

use App\Error\Exception\ValidationErrorException;
use Cake\Error\ExceptionRenderer;

class AppExceptionRenderer extends ExceptionRenderer
{
    public function validationError(ValidationErrorException $exception)
    {
        $code = $this->_code($exception);
        $method = $this->_method($exception);
        $template = $this->_template($exception, $method, $code);

        $message = $this->_message($exception, $code);
        $url = $this->controller->request->getRequestTarget();

        $response = $this->controller->getResponse();
        foreach ((array)$exception->responseHeader() as $key => $value) {
            $response = $response->withHeader($key, $value);
        }
        $this->controller->setResponse($response->withStatus($code));

        $viewVars = [
            'message' => $message,
            'url' => h($url),
            'error' => $exception,
            'code' => $code,
            'errors' => $exception->getValidationErrors(),
            /*'_serialize' => [
                'message',
                'url',
                'code',
                'errors'
            ]*/
        ];
        $this->controller->set($viewVars);
        $this->controller->viewBuilder()->setOption('serialize', [
            'message',
            'url',
            'code',
            'errors'
        ]);

        return $this->_outputMessage($template);
    }
}
