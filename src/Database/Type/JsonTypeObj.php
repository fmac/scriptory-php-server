<?php

namespace App\Database\Type;

use Cake\Database\Type\JsonType;

class JsonTypeObj extends JsonType
{
    public function toPHP($value, \Cake\Database\DriverInterface $driver)
    {
        if ($value === null) {
            return null;
        }

        return json_decode($value, false);
    }
}
