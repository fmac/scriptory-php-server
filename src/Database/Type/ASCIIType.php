<?php

namespace App\Database\Type;

use Cake\Database\Type\StringType;

class ASCIIType extends StringType
{
    public function toDatabase($value, \Cake\Database\DriverInterface $driver): ?string
    {
        return iconv("UTF-8", "ASCII//IGNORE", $value);
    }
}
