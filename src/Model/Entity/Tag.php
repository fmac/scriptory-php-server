<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Class Tag
 *
 * @property int id
 * @property string name
 */
class Tag extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * @param string $text Tag text
     * @return mixed
     */
    protected function _setText($text)
    {
        return trim($text);
    }
}
