<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Class Widget
 *
 * @property string id
 * @property string uid
 * @property array config
 * @property array layout
 * @property string board_id
 * @property string script_id
 * @property Board board
 */
class Widget extends Entity
{
    protected $_accessible = [
        '*' => true,
    ];
}
