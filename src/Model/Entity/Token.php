<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Class Token
 *
 * @property int id
 * @property string token
 * @property string type
 * @property \Cake\I18n\FrozenTime expires
 * @property bool force_expired
 * @property \Cake\I18n\FrozenTime created
 * @property int user_id
 */
class Token extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
