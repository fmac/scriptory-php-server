<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Class Keypair
 *
 * @property int id
 * @property string type
 * @property string purpose
 * @property bool valid
 * @property string pub_key
 * @property string rev_cert
 * @property string enc_priv_key
 * @property string salt
 * @property \Cake\I18n\FrozenTime created
 * @property int user_id
 */
class Keypair extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'user_id' => false
    ];

    /**
     * Set keypair validity field
     *
     * @param bool $valid Valid
     * @return void
     */
    public function setValid($valid)
    {
        $this->set('valid', $valid);
    }
}
