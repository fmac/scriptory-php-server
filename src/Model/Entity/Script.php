<?php

namespace App\Model\Entity;

use Cake\Collection\Collection;
use Cake\ORM\Entity;

/**
 * Class Script
 *
 * @property string id
 * @property string uid
 * @property \Cake\I18n\FrozenTime created
 * @property \Cake\I18n\FrozenTime modified
 * @property string version
 * @property string name
 * @property bool public
 * @property array sandbox
 * @property array dimensions
 * @property int options
 * @property int user_id
 * @property User user
 */
class Script extends Entity
{
    public static $BOARD_CTL_WIDGET_UID = 'sboardctl';

    protected $_accessible = [
        '*' => true,
    ];

    /**
     * Create tags string.
     *
     * @return string
     */
    protected function _getTagsString()
    {
        if (isset($this->_fields['tags_string'])) {
            return $this->_fields['tags_string'];
        }
        if (empty($this->tags)) {
            return '';
        }
        $tags = new Collection($this->tags);
        $str = $tags->reduce(function ($string, $tag) {
            return $string . $tag->text . ',';
        }, '');

        return trim($str, ',');
    }
}
