<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Utility\Hash;

/**
 * Class User
 *
 * @property int id
 * @property string username
 * @property string email
 * @property int pw_v
 * @property string salt
 * @property string modulus_id
 * @property string verifier
 * @property bool verified
 * @property FrozenTime created
 * @property int quota
 * @property string secrets
 * @property array config
 * @property int group_id
 */
class User extends Entity
{
    protected $_accessible = [
        '*' => true
    ];

    /**
     * Check if the user has any rule errors.
     *
     * @return bool
     */
    public function hasRuleErrors()
    {
        $ruleErrorPaths = [
            'email.unique',
            'username.unique'
        ];

        $errors = $this->getErrors();

        foreach ($ruleErrorPaths as $errorPath) {
            if (Hash::get($errors, $errorPath) !== null) {
                return true;
            }
        }

        return false;
    }
}
