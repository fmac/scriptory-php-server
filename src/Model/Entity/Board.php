<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Class Board
 *
 * @property string id
 * @property string uid
 * @property string name
 * @property string description
 * @property \Cake\I18n\FrozenTime created
 * @property \Cake\I18n\FrozenTime modified
 * @property bool public
 * @property int user_id
 * @property User user
 * @property array scripts
 */
class Board extends Entity
{
    protected $_accessible = [
        '*' => true,
    ];
}
