<?php

namespace App\Model\Table;

use App\Model\Entity\Token;
use App\Model\Entity\User;
use Cake\Database\Schema\TableSchemaInterface;
use Cake\I18n\Time;
use Cake\ORM\Table;
use Cake\Utility\Security;

class TokensTable extends Table
{
    const TYPE_ACCESS = 'access';
    const TYPE_RESET_PASSWORD = 'reset_password';
    const TYPE_VERIFY_EMAIL = 'verify_email';
    const TYPE_CHANGE_EMAIL = 'change_email';
    const TYPE_CHANGE_PASSWORD = 'change_password';

    /**
     * {@inheritdoc}
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->belongsTo('Users');
    }

    /**
     * {@inheritdoc}
     */
    protected function _initializeSchema(TableSchemaInterface $schema): TableSchemaInterface
    {
        $schema->setColumnType('id', 'binaryuuid');
        $schema->setColumnType('info', 'jsonobj');
        $schema->setColumnType('user_id', 'binaryuuid');

        return $schema;
    }

    /**
     * Create a new token for the given $user and $type.
     *
     * @param User $user Owner
     * @param string $type Type
     * @param string $validFor date interval period
     * @param array $info Info
     * @return bool|Token
     * @throws \Exception
     */
    public function create(User $user, $type, $validFor = 'PT1H', $info = null)
    {
        $token = $this->newEntity([
            'token' => $this->generateUniqueToken(),
            'type' => $type,
            'expires' => (new \DateTime())->add(new \DateInterval($validFor)),
            'force_expired' => false,
            'created' => Time::now()->format('Y-m-d H:i:s'),
            'info' => $info,
            'user_id' => $user->id
        ]);

        return $this->save($token);
    }

    /**
     * Expire a token matching the given $token string.
     *
     * @param string $token Token
     * @return int
     */
    public function expireToken($token)
    {
        return $this->updateAll(
            ['force_expired' => true],
            ['token' => $token]
        );
    }

    /**
     * Expire all token of a specific $type for the given $user.
     *
     * @param string $userId
     * @param string $type
     * @param array $options
     * @return int
     */
    public function expireTokens($userId, $type, $options = [])
    {
        $cond = ['user_id' => $userId, 'type' => $type];

        if (isset($options['without'])) {
            $cond = array_merge($cond, ['token NOT IN' => $options['without']]);
        }

        return $this->updateAll(
            ['force_expired' => true],
            $cond
        );
    }

    /**
     * Renew the token expiration date of the given $token.
     *
     * @param string $token
     * @param string $validFor Date interval period the token should be valid for from now.
     * @throws \Exception
     * @return void
     */
    public function renewTokenExpiration($token, $validFor = 'PT1H')
    {
        $this->updateAll(
            ['expires' => (new \DateTime())->add(new \DateInterval($validFor))],
            ['token' => $token]
        );
    }

    /**
     * Generate a unique token string.
     *
     * @return string
     */
    public function generateUniqueToken()
    {
        $token = $this->generateToken();

        while ($this->exists(['token' => $token])) {
            $token = $this->generateToken();
        }

        return $token;
    }

    /**
     * Generate a token string.
     *
     * @return string
     */
    public function generateToken()
    {
        return bin2hex(hash('sha512', Security::randomBytes(64), true));
    }
}
