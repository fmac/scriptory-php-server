<?php

namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\Core\Configure;
use Cake\Database\Schema\TableSchemaInterface;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Class UsersTable
 *
 * @property TokensTable Tokens
 * @property KeypairsTable Keypairs
 * @property TokensTable Boards
 * @property KeypairsTable Scripts
 */
class UsersTable extends Table
{
    public static $validUsernameRegEx = '/^[a-zA-Z0-9](_(?!(\.|_))|\.(?!(_|\.))|[a-zA-Z0-9]){1,28}[a-zA-Z0-9]$/';
    public static $validEmailRegEx = "/^[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/";

    /**
     * {@inheritdoc}
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->hasMany('Keypairs');
        $this->hasMany('Tokens');
        $this->hasMany('Boards');
        $this->hasMany('Scripts');
    }

    /**
     * {@inheritdoc}
     */
    protected function _initializeSchema(TableSchemaInterface $schema): TableSchemaInterface
    {
        $schema->setColumnType('id', 'binaryuuid');
        $schema->setColumnType('username', 'ascii');
        $schema->setColumnType('email', 'ascii');
        $schema->setColumnType('config', 'jsonobj');

        return $schema;
    }

    /**
     * {@inheritdoc}
     */
    protected function _requireEmail(Validator $validator, $mode = true)
    {
        $validator
            ->requirePresence('email', $mode, __('The field "email" must be present.'));

        return $validator;
    }

    /**
     * Require presence of username for the given validator instance.
     *
     * {@inheritdoc}
     */
    protected function _requireUsername(Validator $validator, $mode = true)
    {
        $validator
            ->requirePresence('username', $mode, __('The field "username" must be present.'));

        return $validator;
    }

    /**
     * Require presence of password version for the given validator instance.
     *
     * {@inheritdoc}
     */
    protected function _requirePasswordVersion(Validator $validator, $mode = true)
    {
        $validator
            ->requirePresence('pw_v', $mode, __('Invalid password version.'));

        return $validator;
    }

    /**
     * Require presence of salt for the given validator instance.
     *
     * {@inheritdoc}
     */
    protected function _requireUserSalt(Validator $validator, $mode = true)
    {
        $validator
            ->requirePresence('salt', $mode, __('Invalid user salt.'));

        return $validator;
    }

    /**
     * Require presence of modulus_id for the given validator instance.
     *
     * {@inheritdoc}
     */
    protected function _requireModulusId(Validator $validator, $mode = true)
    {
        $validator
            ->requirePresence('modulus_id', $mode, __('No modulus id given.'));

        return $validator;
    }

    /**
     * Require presence of verifier for the given validator instance.
     *
     * {@inheritdoc}
     */
    protected function _requireVerifier(Validator $validator, $mode = true)
    {
        $validator
            ->requirePresence('verifier', $mode, __('No verifier given.'));

        return $validator;
    }

    /**
     * Add email validation to the given validator instance.
     *
     * {@inheritdoc}
     */
    protected function _validEmail(Validator $validator)
    {
        $validator
            ->allowEmptyString('email', __('Please enter an email address.'), false)
            ->add('email', [
                'valid' => [
                    'rule' => ['email'],
                    'message' => __('Please enter a valid email address'),
                ]
            ]);

        return $validator;
    }

    /**
     * Add username validation to the given validator instance.
     *
     * {@inheritdoc}
     */
    protected function _validUsername(Validator $validator)
    {
        $validator
            ->allowEmptyString('username', __('Please enter a username.'), false)
            ->add('username', [
                'minLength' => [
                    'rule' => ['minLength', 3],
                    'message' => __('The username must contain at least 3 characters.'),
                ]
            ])
            ->add('username', 'validFormat', [
                'rule' => function ($value) {
                    return (bool)preg_match(self::$validUsernameRegEx, $value);
                },
                'message' => __('Invalid username format.')
            ]);

        return $validator;
    }

    /**
     * Add password version validation to the given validator instance.
     *
     * {@inheritdoc}
     */
    protected function _validPasswordVersion(Validator $validator)
    {
        $validator
            ->allowEmptyString('pw_v', __('Invalid password version.'), false)
            ->add('pw_v', 'validFormat', [
                'rule' => function ($value) {
                    return in_array($value, [1], true);
                },
                'message' => __('Invalid password version.')
            ]);

        return $validator;
    }

    /**
     * Add user salt validation to the given validator instance.
     *
     * {@inheritdoc}
     */
    protected function _validUserSalt(Validator $validator)
    {
        $validator
            ->allowEmptyString('salt', __('Invalid user salt.'), false)
            ->add('salt', 'validFormat', [
                'rule' => function ($value) {
                    return (bool)preg_match('/^(?:[A-Za-z0-9+\/]{4})*(?:[A-Za-z0-9+\/]{2}==|[A-Za-z0-9+\/]{3}=)?$/', $value);
                },
                'message' => __('Invalid user salt.')
            ]);

        return $validator;
    }

    /**
     * Add modulus id validation to the given validator instance.
     *
     * {@inheritdoc}
     */
    protected function _validModulusId(Validator $validator)
    {
        $validator
            ->allowEmptyString('modulus_id', __('No modulus id given.'), false)
            ->add('modulus_id', 'validFormat', [
                'rule' => function ($value) {
                    return in_array($value, array_keys(Configure::read('SRPModulus')), true);
                },
                'message' => __('Invalid modulus id version.')
            ]);

        return $validator;
    }

    /**
     * Add verifier validation to the given validator instance.
     *
     * {@inheritdoc}
     */
    protected function _validVerifier(Validator $validator)
    {
        $validator
            ->allowEmptyString('verifier', __('No verifier given.'), false)
            ->add('verifier', [
                'lengthBetween' => [
                    'rule' => ['lengthBetween', 64, 1024],
                    'message' => __('Invalid verifier.'),
                ],
            ])
            ->add('verifier', 'validFormat', [
                'rule' => function ($value) {
                    return (bool)preg_match('/^(?:[A-Za-z0-9+\/]{4})*(?:[A-Za-z0-9+\/]{2}==|[A-Za-z0-9+\/]{3}=)?$/', $value);
                },
                'message' => __('Invalid verifier.')
            ]);

        return $validator;
    }

    /**
     * {@inheritdoc}
     */
    protected function _validSecrets(Validator $validator)
    {
        $validator
            ->allowEmptyString('secrets', true)
            ->add('secrets', [
                'lengthBetween' => [
                    'rule' => ['lengthBetween', 0, 65535],
                    'message' => __('Invalid secrets string.'),
                ],
            ])
            ->add('secrets', 'validFormat', [
                'rule' => function ($value) {
                    return (bool)preg_match('/^-----BEGIN PGP MESSAGE-----/', $value);
                },
                'message' => __('Invalid secrets format.')
            ]);

        return $validator;
    }

    /**
     * {@inheritdoc}
     */
    protected function _validConfig(Validator $validator)
    {
        $validator
            ->allowEmptyArray('config', true)
            ->add('config', 'validFormat', [
                'rule' => function ($value, $context) {
                    if ((gettype($value) === 'array' && count(array_diff(array_keys($value), ['defaultKp', 'defaultSP'])) > 0) || strlen(json_encode($value)) > 8096) {
                        return false;
                    }

                    return true;
                },
                'message' => __('Invalid config setting.')
            ]);

        return $validator;
    }

    /**
     * Signup validation configuration.
     *
     * {@inheritdoc}
     */
    public function validationSignup(Validator $validator)
    {
        $validator = $this->_requireEmail($validator);
        $validator = $this->_requireUsername($validator);
        $validator = $this->_requirePasswordVersion($validator);
        $validator = $this->_requireUserSalt($validator);
        $validator = $this->_requireModulusId($validator);
        $validator = $this->_requireVerifier($validator);

        $validator = $this->_validEmail($validator);
        $validator = $this->_validUsername($validator);
        $validator = $this->_validPasswordVersion($validator);
        $validator = $this->_validUserSalt($validator);
        $validator = $this->_validModulusId($validator);
        $validator = $this->_validVerifier($validator);

        return $validator;
    }

    /**
     * Reset password validation configuration.
     *
     * {@inheritdoc}
     */
    public function validationResetPassword(Validator $validator)
    {
        $validator = $this->_requirePasswordVersion($validator);
        $validator = $this->_requireUserSalt($validator);
        $validator = $this->_requireModulusId($validator);
        $validator = $this->_requireVerifier($validator);

        $validator = $this->_validPasswordVersion($validator);
        $validator = $this->_validUserSalt($validator);
        $validator = $this->_validModulusId($validator);
        $validator = $this->_validVerifier($validator);
        $validator = $this->_validConfig($validator);

        return $validator;
    }

    /**
     * Update validation configuration.
     *
     * {@inheritdoc}
     */
    public function validationUpdate(Validator $validator)
    {
        $validator = $this->_validPasswordVersion($validator);
        $validator = $this->_validUserSalt($validator);
        $validator = $this->_validModulusId($validator);
        $validator = $this->_validVerifier($validator);
        $validator = $this->_validSecrets($validator);
        $validator = $this->_validConfig($validator);

        return $validator;
    }

    /**
     * Rules configuration.
     *
     * {@inheritdoc}
     */
    public function buildRules(RulesChecker $rules): \Cake\ORM\RulesChecker
    {
        $rules->addCreate([$this, 'isUniqueEmail'], 'unique', [
            'errorField' => 'email',
            'message' => __('This email is already in use.')
        ]);

        $rules->addCreate([$this, 'isUniqueUsername'], 'unique', [
            'errorField' => 'username',
            'message' => __('This username is already taken.')
        ]);

        return $rules;
    }

    /**
     * @param User $user User entity
     * @param array $data Data to patch
     * @param bool $deleteUnverified Allow overwrite existing user if unverified
     * @return bool|User|mixed
     */
    public function create(User $user, array $data, $deleteUnverified = false)
    {
        $uU = $this->find('byUsernameOrEmail', ['user' => $data['username']])->first();
        $uE = $this->find('byUsernameOrEmail', ['user' => $data['email']])->first();
        if ($uU || $uE) {
            /** @var User $uU */
            /** @var User $uE */
            if ($deleteUnverified && (!$uU || !$uU->verified) && (!$uE || !$uE->verified)) {
                if ($uU) {
                    $this->delete($uU);
                }
                if ($uE) {
                    $this->delete($uE);
                }
            } else {
                return false;
            }
        }

        $user = $this->patchEntity($user, $data, [
            'validate' => 'signup',
            'fieldList' => ['username', 'email', 'pw_v', 'salt', 'modulus_id', 'verifier', 'created', 'quota', 'secrets', 'config', 'group_id', 'keypairs', 'tokens'],
            'associated' => [
                'Keypairs' => [
                    'validate' => 'creation',
                    'fieldList' => ['number', 'type', 'purpose', 'valid', 'pub_key', 'rev_cert', 'enc_priv_key', 'salt', 'created']],
                'Tokens']
        ]);

        return $this->save($user);
    }

    /**
     * {@inheritdoc}
     */
    public function findByToken(Query $query, array $options)
    {
        return $query->matching('Tokens', function (Query $query) use ($options) {
            return $query->where([
                'Tokens.token' => $options['token'],
                'Tokens.type' => $options['type'],
                'Tokens.force_expired' => false,
                'Tokens.expires >' => new \DateTime()
            ]);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function findByUsernameOrEmail(Query $query, array $options)
    {
        $u = $options['user'];
        if (gettype($u) === 'string') {
            return $query->where(['OR' => ['Users.username' => $u, 'Users.email' => $u]]);
        } else {
            return $query->where(['OR' => ['Users.username' => $u->username, 'Users.email' => $u->email]]);
        }
    }

    /**
     * Verify the user with the given $userId.
     *
     * @param string $userId User id
     * @return void
     */
    public function verify($userId)
    {
        $this->updateAll(['verified' => 1], ['id' => $userId]);
    }

    /**
     * Check if the username of the given $user is unique.
     *
     * @param User $user User
     * @return bool
     */
    public function isUniqueUsername(User $user)
    {
        return !$this->exists(['Users.username' => $user->username]);
    }

    /**
     * Check if the email of the given $user is unique.
     *
     * @param User $user User
     * @return bool
     */
    public function isUniqueEmail(User $user)
    {
        return !$this->exists(['Users.email' => $user->email]);
    }

    /**
     * Check if the given username is used.
     *
     * @param string $username Username
     * @return bool
     */
    public function usernameAvailable($username)
    {
        return !$this->exists(['Users.username' => $username, 'Users.verified' => 1]);
    }

    /**
     * Check if the given email is used.
     *
     * @param string $email Email
     * @return bool
     */
    public function emailAvailable($email)
    {
        return !$this->exists(['Users.email' => $email, 'Users.verified' => 1]);
    }

    /**
     * Retrieve user info.
     *
     * @param int $userId User id
     * @return array|\Cake\Datasource\EntityInterface|null
     */
    /*public function info($userId)
    {
        return $this->find()
            ->select(['Users.username', 'Users.email', 'Users.created', 'Users.group_id'])
            ->where(['Users.id' => $userId])
            ->first();
    }*/

    /**
     * Retrieve profile data by username
     *
     * @param string $username Username
     * @param bool $private Include user's private data
     * @return array|\Cake\Datasource\EntityInterface|null
     */
    public function profile($username, $private = false)
    {
        $select = ['Users.username', 'Users.created'];

        if ($private) {
            $select = array_merge($select, ['Users.email']);
        }
        $data = $this->find()
            ->select($select)
            ->where(['Users.username' => $username])
            ->first();
        if (!$data) {
            return null;
        }

        $data['flag'] = self::flag($username);

        return $data;
    }

    /**
     * @param string $username Username
     * @return array
     */
    public static function flag(string $username)
    {
        $md5 = md5($username);
        
        return [
            '#' . substr($md5, -24, 6),
            '#' . substr($md5, -18, 6),
            '#' . substr($md5, -12, 6),
            '#' . substr($md5, -6)
        ];
    }

    /**
     * Return available user's quota in bytes.
     *
     * @param User $u User
     * @return float|int|null
     */
    /*public function quotaLeft(User $u)
    {
        $q = $u->quota;
        $folder = new Folder(USERS_FILES . $u->username);
        if (!$q || !$folder) {
            return null;
        }

        return $q * 1048576 - ($folder->path ? $folder->dirsize() : 0);
    }*/
}
