<?php

namespace App\Model\Table;

use App\Model\Entity\Token;
use App\Model\Entity\User;
use Cake\Database\Schema\TableSchemaInterface;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class KeypairsTable extends Table
{
    const TYPE_DEFAULT = 'ed25519';
    const PURPOSE_DEFAULT = 'generic';
    const VALID_SALT_REGEX = '/^(?:[A-Za-z0-9+\/]{4})*(?:[A-Za-z0-9+\/]{2}==|[A-Za-z0-9+\/]{3}=)?$/';

    /**
     * {@inheritdoc}
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->belongsTo('Users');
    }

    /**
     * {@inheritdoc}
     */
    protected function _initializeSchema(TableSchemaInterface $schema): TableSchemaInterface
    {
        $schema->setColumnType('id', 'binaryuuid');

        return $schema;
    }

    /**
     * {@inheritdoc}
     */
    protected function _validNumber(Validator $validator)
    {
        $validator
            ->allowEmptyString('number', __('No number provided.'), false)
            ->add('number', 'validFormat', [
                'rule' => function ($value) {
                    return $value >= 0 && $value <= 65535;
                },
                'message' => __('Invalid number.')
            ]);

        return $validator;
    }

    /**
     * {@inheritdoc}
     */
    protected function _validType(Validator $validator)
    {
        $validator
            ->allowEmptyString('type', __('No type provided.'), false)
            ->add('type', 'validFormat', [
                'rule' => function ($value) {
                    return in_array($value, ['ed25519'], true);
                },
                'message' => __('Invalid type.')
            ]);

        return $validator;
    }

    /**
     * {@inheritdoc}
     */
    protected function _validPurpose(Validator $validator)
    {
        $validator
            ->allowEmptyString('purpose', __('No purpose provided.'), false);

        return $validator;
    }

    /**
     * {@inheritdoc}
     */
    protected function _validSalt(Validator $validator)
    {
        $validator
            ->allowEmptyString('salt', __('No salt provided.'), false)
            ->add('salt', 'validFormat', [
                'rule' => function ($value) {
                    return (bool)preg_match(self::VALID_SALT_REGEX, $value);
                },
                'message' => __('Invalid salt.')
            ]);

        return $validator;
    }

    /**
     * {@inheritdoc}
     */
    public function validationCreation(Validator $validator)
    {
        $validator
            ->requirePresence('number', true, __('No number provided.'))
            ->requirePresence('type', true, __('No type provided.'))
            ->requirePresence('purpose', true, __('No purpose provided.'))
            ->requirePresence('pub_key', true, __('No public key provided.'))
            ->requirePresence('rev_cert', true, __('No revocation certificate provided.'))
            ->requirePresence('enc_priv_key', true, __('No private key provided.'))
            ->requirePresence('salt', true, __('No salt provided.'));
        $validator = $this->_validNumber($validator);
        $validator = $this->_validType($validator);
        $validator = $this->_validPurpose($validator);
        $validator = $this->_validSalt($validator);

        return $validator;
    }

    /**
     * {@inheritdoc}
     */
    public function validationUpdate(Validator $validator)
    {
        $validator
            ->requirePresence('number', true, __('No number provided.'))
            ->requirePresence('type', true, __('No type provided.'))
            ->requirePresence('purpose', true, __('No purpose provided.'))
            ->requirePresence('pub_key', true, __('No public key provided.'))
            ->requirePresence('rev_cert', true, __('No revocation certificate provided.'))
            ->requirePresence('enc_priv_key', true, __('No private key provided.'))
            ->requirePresence('salt', true, __('No salt provided.'));
        $validator = $this->_validNumber($validator);
        $validator = $this->_validType($validator);
        $validator = $this->_validPurpose($validator);
        $validator = $this->_validSalt($validator);

        return $validator;
    }

    /**
     * Create a new keypair for the given $user and $purpose.
     *
     * @param User $user
     * @param string $purpose
     * @param boolean $valid
     * @param string $pub_key
     * @param string $enc_priv_key
     * @param string $salt
     * @return bool|Token
     * @throws \Exception
     */
    /*public function create(User $user, $purpose, $valid, $pub_key, $enc_priv_key, $salt)
    {
        $keypair = $this->newEntity([
            'user_id' => $user->id,
            'purpose' => $purpose,
            'valid' => $valid,
            'pub_key' => $pub_key,
            'enc_priv_key' => $enc_priv_key,
            'salt' => $salt,
            'created' => Time::now()->format('Y-m-d H:i:s')
        ]);

        return $this->save($keypair);
    }*/

    /**
     * {@inheritdoc}
     */
    public function findByUsername(Query $query, array $options)
    {
        return $query->innerJoinWith('Users', function (Query $q) use ($options) {
            return $q
                ->where(['Users.username' => $options['username']]);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function findByWidgets(Query $query, array $options)
    {
        return $query->contain('Widgets', function (Query $q) use ($options) {
            return $q
                ->where(['Widgets.uid IN' => $options['widgets']]);
        });
    }
}
