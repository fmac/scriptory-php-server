<?php

namespace App\Model\Table;

use App\Model\Entity\Board;
use App\Model\Entity\Script;
use App\Model\Entity\User;
use Cake\Database\Schema\TableSchemaInterface;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Class WidgetsTable
 *
 * @property BoardsTable Boards
 * @property ScriptsTable Scripts
 */
class WidgetsTable extends Table
{
    public static $validUidRegEx = '/^w[0-9a-z]{1,128}$/';

    /**
     * {@inheritdoc}
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->belongsTo('Boards');
        $this->belongsTo('Scripts');
    }

    /**
     * {@inheritdoc}
     */
    protected function _initializeSchema(TableSchemaInterface $schema): TableSchemaInterface
    {
        $schema->setColumnType('id', 'binaryuuid');
        $schema->setColumnType('config', 'jsonobj');
        $schema->setColumnType('layout', 'jsonobj');

        return $schema;
    }

    /**
     * Create a unique board id.
     *
     * @param int $length Length
     * @return string|null
     */
    protected function _createUId($length = 6)
    {
        if ($length < 1) {
            return null;
        }

        $randomString = null;
        do {
            $characters = '01234567890123456789abcdefghijklmnopqrstuvwxyz';
            $charactersLength = strlen($characters);
            $randomString = 'w';
            for ($i = 0; $i < $length - 1; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }

        } while ($this->findByUid($randomString)->first());

        return $randomString;
    }

    /**
     * @param Validator $validator Validator
     * @return Validator
     */
    protected function _validConfig(Validator $validator)
    {
        $validator
            ->allowEmptyArray('config', true)
            ->add('config', 'validFormat', [
                'rule' => function ($value, $context) {
                    if ((gettype($value) === 'array' && count(array_diff(array_keys($value), ['okToRun', 'sP'])) > 0) || strlen(json_encode($value)) > 8096) {
                        return false;
                    }

                    return true;
                },
                'message' => __('Invalid config setting.')
            ]);

        return $validator;
    }

    /**
     * @param Validator $validator Validator
     * @return Validator
     */
    protected function _validLayout(Validator $validator)
    {
        $validator
            ->allowEmptyString('layout', __('No layout provided.'), false)
            ->add('layout', 'validFormat', [
                'rule' => function ($value, $context) {
                    if ((gettype($value) !== 'array')) {
                        return false;
                    }
                    if (count(array_diff(array_keys($value), ['w', 'h', 'x', 'y'])) > 0) {
                        return false;
                    }
                    if ($value['w'] < 1 || $value['w'] > 12 || $value['h'] < 1 || $value['h'] > 1000 ||
                        $value['x'] < 0 || $value['x'] > 11 || $value['y'] < 0 || $value['y'] > 1000) {
                        return false;
                    }

                    return true;
                },
                'message' => __('Invalid layout provided.')
            ]);

        return $validator;
    }

    /**
     * Edit validation configuration.
     *
     * @param Validator $validator Validator
     * @return Validator
     */
    public function validationEdit(Validator $validator)
    {
        $validator
            ->requirePresence('config', false)
            ->requirePresence('layout', false);
        $validator = $this->_validConfig($validator);
        $validator = $this->_validLayout($validator);

        return $validator;
    }

    /**
     * @param Board $b Owner board
     * @param Script $s Script
     * @return \Cake\Datasource\EntityInterface
     */
    public function create(Board $b, Script $s)
    {
        return $this->newEntity([
            'uid' => $this->_createUId(6),
            'config' => (object)[],
            'layout' => (object)[],
            'board_id' => $b->id,
            'script_id' => $s->id
        ]);
    }

    /**
     * @param array $data Layouts
     * @param User $user If set only updates widgets that belong to $user
     * @return bool
     * @throws \Exception
     */
    public function saveLayouts(array $data, $user = null)
    {
        $uids = array_map(function ($w) {
            return isset($w['i']) ? $w['i'] : null;
        }, $data);
        if (in_array(null, $uids)) {
            return false;
        }

        $ww = $this->find()
            ->where(['Widgets.uid IN' => $uids]);
        if (!empty($user)) {
            $ww = $ww->matching('Boards', function (Query $q) use ($user) {
                return $q->where(['Boards.user_id' => $user->id]);
            });
        }
        $ww = $ww->toArray();

        $ww = array_map(function ($w) use ($data) {
            /** @var array $matchingLayout */
            $matchingLayout = array_values(array_filter($data, function ($l) use ($w) {
                return $l['i'] === $w['uid'];
            }))[0];
            $w['layout'] = array_filter($matchingLayout, function ($k) {
                return in_array($k, ['x', 'y', 'w', 'h']);
            }, ARRAY_FILTER_USE_KEY);

            return $w;
        }, $ww);

        return $this->saveMany($ww, ['validate' => 'edit', 'fieldList' => ['config', 'layout']]);
    }

    /**
     * @param array $data Widgets infos
     * @param User $user If set only updates widgets that belong to $user
     * @return bool
     * @throws \Exception
     */
    public function saveConfigs(array $data, $user = null)
    {
        $uids = array_map(function ($w) {
            return $w['widget_uid'];
        }, $data);

        $ww = $this->find()->where(['Widgets.uid IN' => $uids]);
        if (!empty($user)) {
            $ww = $ww->matching('Boards', function (Query $q) use ($user) {
                return $q->where(['Boards.user_id' => $user->id]);
            });
        }
        $ww = $ww->toArray();

        $ww = array_map(function ($w) use ($data) {
            /** @var array $matchingLayout */
            $matchingLayout = array_values(array_filter($data, function ($l) use ($w) {
                return $l['i'] === $w['uid'];
            }))[0];
            $w['layout'] = array_filter($matchingLayout, function ($k) {
                return in_array($k, ['x', 'y', 'w', 'h']);
            }, ARRAY_FILTER_USE_KEY);

            return $w;
        }, $ww);

        return $this->saveMany($ww, ['validate' => 'edit', 'fieldList' => ['config', 'layout']]);
    }

    /**
     * @param array $data Widgets data
     * @param User $user If set only updates widgets that belong to $user
     * @return bool
     * @throws \Exception
     */
    public function saveData(array $data, $user = null)
    {
        $uids = array_map(function ($w) {
            return $w['uid'];
        }, $data);

        $ww = $this->find()
            ->where(['Widgets.uid IN' => $uids]);
        if (!empty($user)) {
            $ww = $ww->matching('Boards', function (Query $q) use ($user) {
                return $q->where(['Boards.user_id' => $user->id]);
            });
        }
        $ww = $ww->toArray();
        if (!$ww) {
            return false;
        }

        $ww = array_map(function ($w) use ($data) {
            $patch = array_values(array_filter($data, function ($d) use ($w) {
                return $d['uid'] === $w['uid'];
            }))[0];
            /** @var array $patch */
            return $this->patchEntity($w, $patch, ['validate' => 'edit', 'fieldList' => ['config', 'layout']]);
        }, $ww);

        return $this->saveMany($ww);
    }

    /**
     * Retrieve widget store file content.
     *
     * @param int $uid Widget uid
     * @return false|string|null
     * @throws \Exception
     */
    /*public function getStorage($uid)
    {
        $w = $this->findByUid($uid);
        if (!$w) {
            throw new \Exception(__('Could not find widget with given id.'));
        }

        $path = $this->_widgetStoreFilePath($w, $w->board->user);
        $f = new File($path);
        if (!$f->exists()) {
            return null;
        }

        return $f->read();
    }*/

    /**
     * Write $storage in widget store file.
     *
     * @param int $uid
     * @param string $storage
     * @return bool
     * @throws \Exception
     */
    /*public function saveStorage($uid, $storage)
    {
        $w = $this->findByUid($uid);
        if (!$w) {
            throw new \Exception(__('Could not find widget with given uid.'));
        }

        $path = $this->_widgetStoreFilePath($w, $w->board->user);
        $f = new File($path, true, 0644);
        if (!$f->exists()) {
            throw new \Exception(__('Could not create widget store file.'));
        }

        return $f->write($storage, 'w');
    }*/

    /**
     * Delete widget store file.
     *
     * @param int $uid
     * @return bool
     * @throws \Exception
     */
    /*public function deleteStorage($uid)
    {
        $w = $this->findByUid($uid);
        if (!$w) {
            throw new \Exception(__('Could not find widget with given uid.'));
        }



    }*/
}
