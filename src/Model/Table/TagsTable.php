<?php

namespace App\Model\Table;

use App\Model\Entity\Tag;
use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class TagsTable extends Table
{
    /**
     * {@inheritdoc}
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->belongsToMany('Scripts', [
            'joinTable' => 'scripts_tags',
        ]);
    }

    /**
     * Tag creation validation configuration.
     *
     * @param Validator $validator
     * @return Validator
     */
    public function validationCreation(Validator $validator)
    {
        /*$validator
            ->requirePresence('text', true, __('No text provided.'));*/
        $validator = $this->_validText($validator);

        return $validator;
    }

    /**
     * Add text validation to the given validator instance.
     *
     * @param Validator $validator
     * @return Validator
     */
    protected function _validText(Validator $validator)
    {
        $validator
            ->allowEmptyString('text', __('No text provided.'), false)
            ->add('text', 'validFormat', [
                'rule' => function ($value, $context) {
                    if (strlen($value) > 256) {
                        return false;
                    }

                    return !in_array($value, []);
                },
                'message' => __('Invalid text.')
            ]);

        return $validator;
    }
}
