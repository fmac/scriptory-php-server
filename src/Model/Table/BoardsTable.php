<?php

namespace App\Model\Table;

use App\Model\Entity\Board;
use App\Model\Entity\User;
use Cake\Database\Schema\TableSchemaInterface;
use Cake\I18n\Time;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Utility\Text;
use Cake\Validation\Validator;

/**
 * Class BoardsTable
 *
 * @property UsersTable Users
 * @property ScriptsTable Scripts
 * @property WidgetsTable Widgets
 */
class BoardsTable extends Table
{
    /**
     * {@inheritdoc}
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->belongsTo('Users');
        $this->belongsToMany('Scripts', [
            'through' => 'Widgets',
            'saveStrategy' => 'append'
        ]);
        $this->hasMany('Widgets');
    }

    /**
     * {@inheritdoc}
     */
    protected function _initializeSchema(TableSchemaInterface $schema): TableSchemaInterface
    {
        $schema->setColumnType('id', 'binaryuuid');
        $schema->setColumnType('config', 'jsonobj');

        return $schema;
    }

    /**
     * Create a unique board id.
     *
     * @param int $length Result string length
     * @return string|null
     */
    protected function _createUId($length = 6)
    {
        if ($length < 1) {
            return null;
        }

        $randomString = null;
        do {
            $characters = '01234567890123456789abcdefghijklmnopqrstuvwxyz';
            $charactersLength = strlen($characters);
            $randomString = 'b';
            for ($i = 0; $i < $length - 1; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }

        } while ($this->findByUid($randomString)->first());

        return $randomString;
    }

    /**
     * {@inheritdoc}
     */
    protected function _validName(Validator $validator)
    {
        $validator
            ->allowEmptyString('name', __('No name provided.'), false)
            ->add('name', [
                'lengthBetween' => [
                    'rule' => ['lengthBetween', 1, 128],
                    'message' => __('The name must be 1 to 128 characters long.'),
                ]
            ]);

        return $validator;
    }

    /**
     * {@inheritdoc}
     */
    protected function _validDescription(Validator $validator)
    {
        $validator
            ->allowEmptyString('description', true)
            ->add('description', [
                'maxLength' => [
                    'rule' => ['maxLength', 2048],
                    'message' => __('The description must be at most 2048 characters long.'),
                ]
            ]);

        return $validator;
    }

    /**
     * {@inheritdoc}
     */
    protected function _validPublic(Validator $validator)
    {
        $validator
            ->add('public', 'validFormat', [
                'rule' => function ($value, $context) {
                    return (bool)in_array($value, [true, false], false);
                },
                'message' => __('Invalid public setting.')
            ]);

        return $validator;
    }

    /**
     * {@inheritdoc}
     */
    protected function _validConfig(Validator $validator)
    {
        $validator
            ->allowEmptyArray('config', true)
            ->add('config', 'validFormat', [
                'rule' => function ($value, $context) {
                    if ((gettype($value) === 'array' && count(array_diff(array_keys($value), ['routes'])) > 0) || strlen(json_encode($value)) > 8096) {
                        return false;
                    }
                    if (isset($value['routes'])) {
                        foreach ($value['routes'] as $v) {
                            if ((!(bool)preg_match(WidgetsTable::$validUidRegEx, $v['source'])) || !in_array(gettype($v['destinations']), ['NULL', 'array'])) {
                                return false;
                            }
                            if (gettype($v['destinations']) === 'array') {
                                foreach ($v['destinations'] as $dest) {
                                    if (gettype($dest) !== 'string' || !(bool)preg_match(WidgetsTable::$validUidRegEx, $dest)) {
                                        return false;
                                    }
                                }
                            }
                        }
                    }

                    return true;
                },
                'message' => __('Invalid config setting.')
            ]);

        return $validator;
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave(\Cake\Event\EventInterface $event, Entity $entity, $options)
    {
        $entity->modified = Time::now()->format('Y-m-d H:i:s');
    }

    /**
     * @param Validator $validator Validator
     * @return Validator
     */
    public function validationEdit(Validator $validator)
    {
        $validator
            ->requirePresence('name', false, __('No name provided.'))
            ->requirePresence('description', false, __('No description provided.'))
            ->requirePresence('public', false)
            ->requirePresence('config', false);
        $validator = $this->_validName($validator);
        $validator = $this->_validDescription($validator);
        $validator = $this->_validPublic($validator);
        $validator = $this->_validConfig($validator);

        return $validator;
    }

    /**
     * {@inheritdoc}
     */
    public function findOwnedBy(Query $query, array $options)
    {
        $username = $options['username'];
        if (gettype($options['username']) !== 'string') {
            $username = $options['username']->username;
        }

        return $query->matching('Users', function (Query $q) use ($username) {
            return $q->where(['Users.username' => $username]);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function findPublic(Query $query)
    {
        return $query->where(['Boards.public' => true]);
    }

    /**
     * Find boards by username.
     *
     * @param string $username Username
     * @param bool $private Include user's private data
     * @return array|\Cake\Datasource\EntityInterface|null
     */
    public function findByUsername($username, $private = false)
    {
        if (empty($username)) {
            return null;
        }

        $list = $this->find()
            ->select(['Boards.uid', 'Boards.name', 'Boards.description', 'Boards.created', 'Boards.modified', 'Boards.public', 'Boards.config', 'Users.username']);
        if (!$private) {
            $list = $list->where(['Boards.public' => true]);
        }
        $list = $list->matching('Users', function (Query $q) use ($username) {
            return $q->where(['Users.username' => $username]);
        })
        ->toArray();

        if ($list) {
            return $list;
        }

        return null;
    }

    /**
     * Patch a new board with default values.
     *
     * @param Board $board The entity to patch
     * @param User $user The owner
     * @param array $options The options
     * @return \Cake\Datasource\EntityInterface|false
     */
    public function create(Board $board, User $user, $options = [])
    {
        if (empty($board) || empty($user)) {
            return false;
        }

        //        ex.
        //        $func = $query->func()->substring_index(
        //            [
        //                new \Cake\Database\Expression\IdentifierExpression('title'),
        //                ' ',
        //                $userInput,
        //            ],
        //            [
        //                null,     // no typecasting for the first argument
        //                'string', // second argument will be bound/casted as string
        //                'integer' // third argument will be bound/casted as integer
        //            ]
        //        );
        $data = [
            'id' => Text::uuid(),
            'uid' => $this->_createUId(6),
            'name' => 'Untitled',
            'description' => '',
            'created' => Time::now()->format('Y-m-d H:i:s'),
            'public' => false,
            'config' => (object)[],
            'user_id' => $this->Users->find('byUsernameOrEmail', ['user' => $user])->first()->id
            // 'user' => $this->Users->find('byUsernameOrEmail', ['user' => $user])->first()->toArray()
            // would be prettier but updates user's fields uselessly
        ];

        $board = $this->patchEntity($board, $data);

        return $this->save($board);
    }

    /**
     * @param Board $board The board to patch
     * @param array $data Data
     * @return \Cake\Datasource\EntityInterface|false|null
     * @throws \Exception
     */
    public function saveEdit(Board $board, array $data)
    {
        if (empty($board) || empty($data)) {
            throw new \Exception(__('No data given.'));
        }

        $board = $this->patchEntity($board, $data, ['validate' => 'edit', 'fieldList' => ['name', 'description', 'public', 'config']]);

        return $this->save($board);
    }
}
