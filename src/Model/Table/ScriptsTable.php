<?php

namespace App\Model\Table;

use App\Model\Entity\Script;
use App\Model\Entity\User;

use Cake\Database\Schema\TableSchemaInterface;
use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\I18n\Time;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Class ScriptsTable
 *
 * @property UsersTable Users
 * @property TagsTable Tags
 */
class ScriptsTable extends Table
{
    public static $userScriptsLimit = 10000;
    public static $validUidRegEx = '/^s[0-9a-z]{1,128}$/';
    protected static $specials = ['sboardctl'];

    /**
     * {@inheritdoc}
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->belongsTo('Users')
            ->setJoinType('INNER');
        $this->belongsToMany('Tags', [
            'joinTable' => 'scripts_tags',
        ]);
        $this->belongsToMany('Boards', [
            'through' => 'Widgets',
            'saveStrategy' => 'append'
        ]);
        $this->hasMany('Widgets');
    }

    /**
     * {@inheritdoc}
     */
    protected function _initializeSchema(TableSchemaInterface $schema): TableSchemaInterface
    {
        $schema->setColumnType('id', 'binaryuuid');
        $schema->setColumnType('sandbox', 'jsonobj');
        $schema->setColumnType('dimensions', 'jsonobj');

        return $schema;
    }

    /**
     * @param int $length Result length
     * @return string|null
     */
    protected function _createUid($length = 6)
    {
        if ($length < 1) {
            return null;
        }

        $randomString = null;
        do {
            $characters = '01234567890123456789abcdefghijklmnopqrstuvwxyz';
            $charactersLength = strlen($characters);
            $randomString = 's';
            for ($i = 0; $i < $length - 1; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }

        } while ($this->findByUid($randomString)->first());

        return $randomString;
    }

    /**
     * Add name validation to the given validator instance.
     *
     * @param Validator $validator Validator
     * @return Validator
     */
    protected function _validName(Validator $validator)
    {
        $validator
            ->allowEmptyString('name', __('No name provided.'), false)
            ->add('name', [
                'lengthBetween' => [
                    'rule' => ['lengthBetween', 3, 64],
                    'message' => __('The name must be 3 to 64 characters long.'),
                ]
            ]);

        return $validator;
    }

    /**
     * Add version validation to the given validator instance.
     *
     * @param Validator $validator Validator
     * @return Validator
     */
    /*protected function _validVersion(Validator $validator)
    {
        $validator
            ->allowEmptyString('version', __('No version provided.'), false)
            ->add('version', [
                'lengthBetween' => [
                    'rule' => ['lengthBetween', 1, 32],
                    'message' => __('The version string must be 1 to 32 characters long.'),
                ]
            ]);

        return $validator;
    }*/

    /**
     * Add public setting validation to the given validator instance.
     *
     * @param Validator $validator Validator
     * @return Validator
     */
    protected function _validPublic(Validator $validator)
    {
        $validator
            ->add('public', 'validFormat', [
                'rule' => function ($value, $context) {
                    return (bool)in_array($value, [true, false], false);
                },
                'message' => __('Invalid public setting.')
            ]);

        return $validator;
    }

    /**
     * Add sandbox validation to the given validator instance.
     *
     * @param Validator $validator Validator
     * @return Validator
     */
    protected function _validSandbox(Validator $validator)
    {
        $validator
            ->allowEmptyString('sandbox', __('No sandbox setting provided.'), false)
            ->add('sandbox', 'validFormat', [
                'rule' => function ($value, $context) {
                    if (count($value) > 11) {
                        return false;
                    }
                    foreach ($value as $key => $val) {
                        if (!(bool)in_array($val, [true, false], true)) {
                            return false;
                        }
                    }

                    return true;
                },
                'message' => __('Invalid sandbox setting.')
            ]);

        return $validator;
    }

    /**
     * @param Validator $validator Validator
     * @return Validator
     */
    protected function _validDimensions(Validator $validator)
    {
        $validator
            ->allowEmptyString('dimensions', __('No dimensions provided.'), false)
            ->add('dimensions', 'validFormat', [
                'rule' => function ($value, $context) {
                    if ((gettype($value) !== 'array')) {
                        return false;
                    }
                    if (count(array_diff(array_keys($value), ['w', 'h'])) > 0) {
                        return false;
                    }
                    if ($value['w'] < 1 || $value['w'] > 12 || $value['h'] < 1 || $value['h'] > 100) {
                        return false;
                    }

                    return true;
                },
                'message' => __('Invalid dimensions provided.')
            ]);

        return $validator;
    }

    /**
     * @return false|string
     */
    protected function _defaultDesc()
    {
        $defFD = new File(USERS_FILES . 'default_desc.html');
        $defFD->open('r');
        $desc = $defFD->read();
        $defFD->close();

        return $desc;
    }

    /**
     * @return false|string
     */
    protected function _defaultCode()
    {
        $defFC = new File(USERS_FILES . 'default_code.html');
        $defFC->open('r');
        $code = $defFC->read();
        $defFC->close();

        return $code;
    }

    /**
     * Create script files.
     *
     * @param Script $script The script
     * @param User $user The owner
     * @param array $options Options ['desc' => (string), 'code' => (string)]
     * @return bool|string|null
     * @throws \Exception
     */
    protected function _createScriptFiles(Script $script, User $user, array $options = [])
    {
        $fol = new Folder(USERS_FILES);
        if (!$fol->path) {
            throw new \Exception(__('Cannot access ' . USERS_FILES . '.'));
        }

        $username = strval($user->username);
        $scriptUid = $script->uid;
        if ((!(bool)preg_match(UsersTable::$validUsernameRegEx, $username)) || (!(bool)preg_match(self::$validUidRegEx, $scriptUid))) {
            return false;
        }

        $path = $username . DS . SCRIPTS_DIR . DS . $scriptUid;
        if (!$fol->cd($path)) {
            if (!$fol->create($path, 0744)) {
                throw new \Exception(__('Cannot create ' . $path . '.'));
            }
            $fol->cd($path);
        }
        $fC = new File($this->scriptFilePath($script, 'code'), true, 0644);
        $fD = new File($this->scriptFilePath($script, 'desc'), true, 0644);
        if (!$fC || !$fD) {
            $fC->delete();
            $fD->delete();
            throw new \Exception(__('Cannot create script files.'));
        }

        if (isset($options['desc']) && gettype($options['desc']) === 'string') {
            $desc = $options['desc'];
        } else {
            $desc = $this->_defaultDesc();
        }
        if (isset($options['code']) && gettype($options['code']) === 'string') {
            $code = $options['code'];
        } else {
            $code = $this->_defaultCode();
        }
        $fD->write($desc, 'w');
        $fD->close();
        $fC->write($code, 'w');
        $fC->close();

        return true;
    }

    /**
     * Return script file content.
     *
     * @param Script $script The script
     * @param string $type File type
     * @return false|string|null
     */
    protected function _readScriptFile(Script $script, $type)
    {
        $path = $this->scriptFilePath($script, $type);
        $f = new File($path);
        if (!$f) {
            return __('Cannot access ' . $path . '.');
        }

        return $f->read();
    }

    /**
     * Overwrite script file content.
     *
     * @param Script $script The script
     * @param string $type File type
     * @param string $data Data to be written
     * @return false|string|null
     */
    protected function _writeScriptFile(Script $script, string $type, string $data)
    {
        $path = $this->scriptFilePath($script, $type);
        $f = new File($path);
        if (!$f) {
            return __('Cannot access ' . $path . '.');
        }

        if (!$f->write($data, 'w')) {
            return false;
        }
        $f->close();

        return true;
    }

    /**
     * Delete script desc and code files from disk.
     *
     * @param Script $script The script
     * @param User $user The owner
     * @return bool
     */
    protected function _deleteScriptFiles(Script $script, User $user)
    {
        /*$paths = [
            $this->widgetFilePath($w, $user, 'desc'),
            $this->widgetFilePath($w, $user, 'code'),
        ];

        $success = true;
        foreach ($paths as $key => $path) {
            $f = new File($path);
            if ($f->exists() && !$f->delete()) { $success = false; }
        }*/

        $username = strval($user->username);
        $scriptUid = $script->uid;
        if (! (bool)preg_match(UsersTable::$validUsernameRegEx, $username) || ! (bool)preg_match(self::$validUidRegEx, $scriptUid)) {
            return false;
        }
        $path = USERS_FILES . $username . DS . SCRIPTS_DIR . DS . $scriptUid;
        $dir = new Folder();
        if (!$dir->cd($path)) {
            // Files dir doesn't exists
            return true;
        }
        // Some other safety checks
        if (($dir->pwd() !== $path) || $dir->errors()) {
            return false;
        }

        return $dir->delete();
    }

    /**
     * Add new tags to the tags table.
     *
     * @param string $tagsString Tag string
     * @return array
     */
    protected function _buildTags($tagsString)
    {
        // Trim tags
        $newTags = array_map('trim', explode(',', $tagsString));
        // Remove all empty tags
        $newTags = array_filter($newTags);
        // Reduce duplicated tags
        $newTags = array_unique($newTags);

        $out = [];
        $query = $this->Tags->find()
            ->where(['Tags.text IN' => $newTags]);

        // Remove existing tags from the list of new tags.
        foreach ($query->extract('text') as $existing) {
            $index = array_search($existing, $newTags);
            if ($index !== false) {
                unset($newTags[$index]);
            }
        }
        // Add existing tags.
        foreach ($query as $tag) {
            $out[] = $tag;
        }
        // Add new tags.
        foreach ($newTags as $tag) {
            $out[] = $this->Tags->newEntity(['text' => $tag]);
        }

        return $out;
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave(\Cake\Event\EventInterface $event, $entity, $options)
    {
        $entity->modified = Time::now()->format('Y-m-d H:i:s');

        if ($entity->tags_string) {
            $entity->tags = $this->_buildTags($entity->tags_string);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findOwnedBy(Query $query, array $options)
    {
        $username = $options['username'];
        if (gettype($options['username']) !== 'string') {
            $username = $options['username']->username;
        }

        return $query->matching('Users', function (Query $q) use ($username) {
            return $q->where(['Users.username' => $username]);
        });
    }

    /**
     * {@inheritdoc}
     */
    public function findPopularity(Query $query, array $options)
    {
        $pop = $this->query()
            ->select(['count' => $query->func()->count('Widgets.uid')])
            ->where(['Scripts.uid' => $options['uid']])
            ->leftJoinWith('Widgets')
            ->group(['Scripts.uid']);

        return $query->select(['popularity' =>
            $this->query()->select([
                $query->newExpr()->addCase([
                    $query->newExpr()->isNull($query->identifier('count'))
                ], [0, $query->identifier('count')])
            ])->from(['pop' => $pop])
        ]);
    }

    /**
     * Find scripts by username.
     *
     * @param string $username Username
     * @param bool $private Include user's private data
     * @return array|\Cake\Datasource\EntityInterface|null
     */
    public function findByUsername($username, $private = false)
    {
        if (empty($username)) {
            return null;
        }

        $list = $this->find()
            ->select(['Scripts.uid', 'Scripts.created', 'Scripts.name', 'Scripts.public', 'Users.username']);
        if (!$private) {
            $list = $list->where(['Scripts.public' => true]);
        }
        $list = $list->matching('Users', function (Query $q) use ($username) {
            return $q->where(['Users.username' => $username]);
        });
        $list = $list->select(['entities' => $list->func()->count('Widgets.uid')])
            ->leftJoinWith('Widgets')
            ->group(['Scripts.uid'])
            ->toArray();

        if ($list) {
            return $list;
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function findPublic(Query $query, array $options)
    {
        return $query->where(['Scripts.public' => true]);
    }

    /**
     * Create tags array from tags string
     *
     * @param Script $s Script
     * @return void
     */
    public function parseTagsString(Script $s)
    {
        $tagsString = $s->tags_string;

        if (empty($tagsString)) {
            $s['tags'] = [];

            return;
        }
        // Trim tags
        $newTags = array_map('trim', explode(',', $tagsString));
        // Remove all empty tags
        $newTags = array_filter($newTags);
        // Reduce duplicated tags
        $newTags = array_unique($newTags);

        $out = [];
        // Add new tags.
        foreach ($newTags as $tag) {
            $out[] = ['text' => $tag];
        }

        $s['tags'] = $out;
    }

    /**
     * @param Script $s Script
     * @param array $filesTypes Array containing types of files to be appended
     * @return void
     */
    public function appendFiles(Script $s, array $filesTypes)
    {
        foreach ($filesTypes as $f) {
            $s[$f] = $this->_readScriptFile($s, $f);
        }
    }

    /**
     * @param Script $s Script
     * @return void
     */
    public function appendTagsString(Script $s)
    {
        $s['tags_string'] = $s->tags_string;
    }

    /**
     * Patch a new script with default values.
     *
     * @param Script $s New Script entity
     * @param User $u The owner
     * @return \Cake\Datasource\EntityInterface|false|null
     * @throws \Exception
     */
    public function create(Script $s, User $u)
    {
        if (empty($s) || empty($u)) {
            return null;
        }

        /*if ($this->Users->quotaLeft($u) < strlen($this->_defaultDesc()) + strlen($this->_defaultCode())) {
            throw new \Exception(__('Quota exceeded.'));
        }*/
        if ($this->userCount($u) > self::$userScriptsLimit) {
            throw new \Exception(__('Quota exceeded.'));
        }

        $s = $this->patchEntity($s, [
            //'id' => $this->find()->func()->unhex([$this->find()->func()->replace([Text::UUID(), '-', ''], ['string', 'string', 'string'])], ['string']),
            'uid' => $this->_createUid(6),
            'created' => Time::now()->format('Y-m-d H:i:s'),
            //'version' => '1.0',
            'name' => 'Untitled',
            'public' => false,
            'sandbox' => ['1' => false, '2' => false, '3' => false, '4' => false, '5' => false, '6' => false, '7' => false, '8' => false, '9' => false, '10' => false, '11' => false],
            'dimensions' => ['w' => 2, 'h' => 3],
            'user_id' => $u->id
        ]);

        $res = $this->save($s);
        if ($res) {
            try {
                $this->_createScriptFiles($s, $u);
            } catch (\Exception $e) {
                $this->delete($res);
                throw new \Exception($e->getMessage());
            }

            return $s;
        } else {
            return null;
        }
    }

    /**
     * Delete a script and relative files by uid.
     *
     * @param string $uid Script uid
     * @return bool|null
     */
    public function deleteByUid($uid)
    {
        if (empty($uid)) {
            return null;
        }

        $s = $this->findByUid($uid)->matching('Users')->first();
        $u = $s->_matchingData['Users'];

        if ($s) {
            if ($this->_deleteScriptFiles($s, $u) && $this->delete($s)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Patch a script with user data values.
     *
     * @param Script $s Script
     * @param User $u The owner
     * @param array $data Data to be persisted
     * @return \Cake\Datasource\EntityInterface|false|null
     * @throws \Exception
     */
    public function saveEdit(Script $s, User $u, $data)
    {
        if (empty($s) || empty($data)) {
            throw new \Exception(__('No data given.'));
        }

        // Simple tags_string validation until better way
        if (strlen($data['tags_string']) > 200) {
            throw new \Exception(__('Tags string too long.'));
        }

        if (strlen($data['desc']) > 8388608) {
            throw new \Exception(__('Description file too big.'));
        }
        if (strlen($data['code']) > 8388608) {
            throw new \Exception(__('Code file too big.'));
        }
        /*$oldDesc = $this->_readScriptFile($s, 'desc');
        $oldCode = $this->_readScriptFile($s, 'code');
        if ($this->Users->quotaLeft($u) + strlen($oldDesc) + strlen($oldCode) < strlen($data['desc']) + strlen($data['code'])) {
            throw new \Exception(__('Quota exceeded.'));
        }*/
        if ($this->userCount($u) > self::$userScriptsLimit) {
            throw new \Exception(__('Quota exceeded.'));
        }
        $this->_writeScriptFile($s, 'desc', $data['desc']);
        $this->_writeScriptFile($s, 'code', $data['code']);

        $s = $this->patchEntity($s, $data, ['validate' => 'edit', 'fieldList' => ['name', 'public', 'sandbox', 'dimensions', 'tags', 'tags_string'], 'associated' => ['Tags' => ['validate' => 'creation']]]);

        return $this->save($s);
    }

    /**
     * Clone a script with user data values.
     *
     * @param Script $s Script to clone
     * @param User $u The owner
     * @return \Cake\Datasource\EntityInterface|false|null
     * @throws \Exception
     */
    public function clone(Script $s, User $u)
    {
        if (empty($s) || empty($u)) {
            throw new \Exception(__('Invalid data given.'));
        }

        $desc = $this->_readScriptFile($s, 'desc');
        $code = $this->_readScriptFile($s, 'code');
        /*if ($this->Users->quotaLeft($u) < strlen($desc) + strlen($code)) {
            throw new \Exception(__('Quota exceeded.'));
        }*/
        if ($this->userCount($u) > self::$userScriptsLimit) {
            throw new \Exception(__('Quota exceeded.'));
        }

        $clone = $this->newEntity([
            'uid' => $this->_createUid(6),
            'created' => Time::now()->format('Y-m-d H:i:s'),
            //'version' => '1.0',
            'name' => '[Clone] ' . $s->name,
            'public' => false,
            'sandbox' => $s->sandbox,
            'dimensions' => $s->dimensions,
            'user_id' => $u->id
        ]);
        $clone['tags'] = $s->tags;

        $res = $this->save($clone);
        if ($res) {
            try {
                $this->_createScriptFiles($clone, $u, ['desc' => $desc, 'code' => $code]);
            } catch (\Exception $e) {
                $this->delete($res);
                throw new \Exception($e->getMessage());
            }

            return $clone;
        } else {
            return false;
        }
    }

    /**
     * Edit validation configuration.
     *
     * @param Validator $validator Validator
     * @return Validator
     */
    public function validationEdit(Validator $validator)
    {
        $validator
            ->requirePresence('name', true, __('No name provided.'))
            //->requirePresence('version', true, __('No version provided.'))
            ->requirePresence('public', true, __('No public option provided.'))
            ->requirePresence('sandbox', true, __('No sandbox setting provided.'))
            ->requirePresence('dimensions', true, __('No dimensions provided.'));
        $validator = $this->_validName($validator);
        //$validator = $this->_validVersion($validator);
        $validator = $this->_validPublic($validator);
        $validator = $this->_validSandbox($validator);
        $validator = $this->_validDimensions($validator);

        return $validator;
    }

    /**
     * Construct a specific script file type path.
     *
     * @param Script $script Script
     * @param string $type File type
     * @return string
     */
    public function scriptFilePath(Script $script, string $type)
    {
        $fName = null;
        switch ($type) {
            case 'desc':
                $fName = 'desc.html';
                break;
            case 'code':
                $fName = 'code.html';
                break;
            default:
                return null;
        }

        $u = TableRegistry::getTableLocator()->get('Users')->find()
            ->matching('Scripts', function (Query $q) use ($script) {
                return $q->where(['Scripts.uid' => $script->uid]);
            })
            ->first();

        $username = strval($u->username);
        $scriptUid = $script->uid;
        if ((!(bool)preg_match(UsersTable::$validUsernameRegEx, $username)) || (!(bool)preg_match(self::$validUidRegEx, $scriptUid))) {
            return null;
        }

        return USERS_FILES . $username . DS . SCRIPTS_DIR . DS . $scriptUid . DS . $fName;
    }

    /**
     * @param int $limit Max results for each list
     * @return array
     */
    public function findNewAndPopular($limit)
    {
        $new = $this->find()
            ->select(['Scripts.uid', 'Scripts.created', 'Scripts.name', 'Scripts.public', 'Scripts.sandbox', 'username' => 'Users.username'])
            ->where(['AND' => ['Scripts.public' => true, 'Scripts.uid NOT IN' => self::$specials]])
            ->matching('Users')
            ->order(['Scripts.created' => 'DESC'])
            ->limit($limit)
            ->toArray();
        $popular = $this->find()
            ->select(['Scripts.uid', 'Scripts.created', 'Scripts.name', 'Scripts.public', 'Scripts.sandbox', 'count' => 'count(Widgets.uid)', 'username' => 'Users.username'])
            ->where(['AND' => ['Scripts.public' => true, 'Scripts.uid NOT IN' => self::$specials]])
            ->matching('Widgets')
            ->matching('Users')
            ->group(['Scripts.uid'])
            ->order(['count(Widgets.uid)' => 'DESC'])
            ->limit($limit)
            ->toArray();

        return compact('new', 'popular');
    }

    /**
     * Count scripts owned by a user
     *
     * @param User $u The owner
     * @return bool|int|null
     */
    public function userCount(User $u)
    {
        if (!$u) {
            return false;
        }

        return $this->find()
            ->where(['Scripts.user_id' => $u->id])
            ->count();
    }
}
