<?php

namespace App\Mailer;

use App\Model\Entity\User;
use Cake\Core\Configure;
use Cake\Mailer\Mailer;
use Cake\Routing\Router;

/**
 * Class UserMailer
 */
class UserMailer extends Mailer
{
    /**
     * @param User $user
     * @param string $token
     * @return \Cake\Mailer\Email
     * @throws \Aura\Intl\Exception
     */
    public function signup(User $user, $token)
    {
        return $this
            ->_prepareEmail($user, __('Welcome to {appName}', ['appName' => Configure::read('appName')]))
            ->setViewVars([
                'user' => $user,
                'verifyEmailUrl' => Router::url('/u/verify-email/' . $token, true),
                'instanceName' => 'Scriptory - ' . Configure::read('appTitle')
            ]);
    }

    /**
     * @param User $user
     * @param string $token
     * @return \Cake\Mailer\Email
     * @throws \Aura\Intl\Exception
     */
    public function verifyEmail(User $user, $token)
    {
        return $this
            ->_prepareEmail($user, __('{appName}: Verify your email address', ['appName' => Configure::read('appName')]))
            ->setViewVars([
                'user' => $user,
                'verifyEmailUrl' => Router::url('/u/verify-email/' . $token, true),
                'instanceName' => 'Scriptory - ' . Configure::read('appTitle')
            ]);
    }

    /**
     * @param User $user
     * @param string $token
     * @return \Cake\Mailer\Email
     * @throws \Aura\Intl\Exception
     */
    public function lostPassword(User $user, $token)
    {
        return $this
            ->_prepareEmail($user, __('{appName}: Reset Password', ['appName' => Configure::read('appName')]))
            ->setViewVars([
                'user' => $user,
                'token' => $token,
                'resetPasswordUrl' => Router::url('/u/reset-password/' . $token, true),
                'instanceName' => 'Scriptory - ' . Configure::read('appTitle')
            ]);
    }

    /**
     * @param User $user
     * @param string $token
     * @param string $newEmail
     * @return \Cake\Mailer\Email
     * @throws \Aura\Intl\Exception
     */
    public function verifyChangeEmail(User $user, $token, $newEmail)
    {
        return $this
            ->_prepareEmail($user, __('{appName}: Verify your email address', ['appName' => Configure::read('appName')]))
            ->setTo($newEmail)
            ->setViewVars([
                'user' => $user,
                'verifyEmailUrl' => Router::url('/u/verify-change-email/' . $token, true),
                'instanceName' => 'Scriptory - ' . Configure::read('appTitle')
            ]);
    }

    /**
     * Prepare the UserMailer Email instance.
     *
     * @param User $user The user entity.
     * @param string $subject The subject for email.
     * @return \Cake\Mailer\Email
     */
    protected function _prepareEmail(User $user, $subject)
    {
        return $this
            ->setTransport('default')
            ->setEmailFormat('both')
            ->setDomain(Configure::read('siteDomain'))
            ->setFrom(Configure::read('appMail'), Configure::read('appName'))
            ->setTo($user->email)
            ->setSubject($subject);
    }
}
