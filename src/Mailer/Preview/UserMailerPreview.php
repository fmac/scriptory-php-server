<?php

namespace App\Mailer\Preview;

use App\Model\Entity\User;
use DebugKit\Mailer\MailPreview;

/**
 * Class UserMailer
 */
class UserMailerPreview extends MailPreview
{
    /**
     * @return \Cake\Mailer\Email
     */
    public function signup()
    {
        $this->loadModel("Users");
        $user = $this->Users->find()->first();

        return $this->getMailer("User")
            ->signup($user, "DUMMY_TOKEN***");
    }

    /**
     * @return \Cake\Mailer\Email
     */
    public function verifyEmail()
    {
        $this->loadModel("Users");
        $user = $this->Users->find()->first();

        return $this->getMailer("User")
            ->verifyEmail($user, "DUMMY_TOKEN***");
    }

    /**
     * @return \Cake\Mailer\Email
     */
    public function lostPassword()
    {
        $this->loadModel("Users");
        $user = $this->Users->find()->first();

        return $this->getMailer("User")
            ->lostPassword($user, "DUMMY_TOKEN***");
    }

    /**
     * @return \Cake\Mailer\Email
     */
    public function verifyChangeEmail()
    {
        $this->loadModel("Users");
        $user = $this->Users->find()->first();

        return $this->getMailer("User")
            ->verifyChangeEmail($user, "DUMMY_TOKEN***", "DUMMY@NEW.EMAIL");
    }
}
