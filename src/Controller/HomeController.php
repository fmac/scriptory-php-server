<?php

namespace App\Controller;

use Cake\Event\Event;

class HomeController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
    }

    /**
     * @param Event $event
     * @return \Cake\Http\Response|void|null
     */
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
    }

    public function index()
    {

    }
}
