<?php

namespace App\Controller\Api;

use App\Model\Entity\Board;
use App\Model\Entity\Script;
use App\Model\Table\UsersTable;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\InternalErrorException;
use Cake\Http\Exception\NotAcceptableException;
use Cake\Http\Exception\NotFoundException;
use Cake\ORM\Query;

/**
 * Class UserController
 *
 * @property \App\Model\Table\BoardsTable Boards
 * @property \App\Model\Table\WidgetsTable Scripts
 * @property \App\Model\Table\WidgetsTable Widgets
 * @property \App\Model\Table\UsersTable Users
 */
class BoardsController extends ApiAppController
{
    /**
     * Initialization hook method.
     *
     * @return void
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadModel('Boards');
        $this->loadModel('Scripts');
        $this->loadModel('Widgets');
        $this->loadModel('Users');

        $this->Guardian->allow([
            'getReference',
            'userList',
            'widgetsList'
        ]);
        $this->Guardian->checkAuthAnyway([
            'getReference',
            'userList',
            'widgetsList'
        ]);
    }

    /**
     * POST
     *
     * @return void
     */
    public function getReference()
    {
        $this->request->allowMethod(['post']);

        $uid = $this->request->getData('uid');
        if (!$uid) {
            throw new BadRequestException(__('No uid provided.'));
        }

        /** @var Board $b */
        $b = $this->Boards->findByUid($uid)->select($this->Boards)->select($this->Users)->contain(['Users'])->first();
        if (!$b) {
            throw new NotFoundException();
        }
        if (!$b->public && $b->user->username !== $this->Guardian->user('username')) {
            throw new NotFoundException();
        }

        $b->user->flag = UsersTable::flag($b->user->username);
        $b = array_filter($b->toArray(), function ($v, $k) {
            return in_array($k, ['uid', 'created', 'name', 'public', 'description', 'user']) ? [$k => $v] : null;
        }, ARRAY_FILTER_USE_BOTH);
        $b['user'] = array_filter($b['user'], function ($v, $k) {
            return in_array($k, ['username', 'flag']) ? [$k => $v] : null;
        }, ARRAY_FILTER_USE_BOTH);
        $this->set('board', $b);
        $this->viewBuilder()->setOption('serialize', ['board']);
    }

    /**
     * Create method
     * GET
     *
     * @return void
     */
    public function create()
    {
        $this->request->allowMethod(['get']);
        /** @var Board $b */
        $b = $this->Boards->newEmptyEntity();
        if ($this->Boards->create($b, $this->Guardian->user())) {
            $s = $this->Scripts->findByUid(Script::$BOARD_CTL_WIDGET_UID)->first();
            $w = $this->Widgets->create($b, $s);
            $s->_joinData = $w;
            $ctl = $this->Boards->getAssociation('Scripts')->link($b, [$s]);
            if (!$ctl) {
                $this->Boards->delete($b);
                throw new InternalErrorException(__('Could not create Board Control.'));
            }
            $b = [
                'uid' => $b->uid,
                'name' => $b->name
            ];
            $this->set('board', $b);
        } else {
            throw new \App\Error\Exception\ValidationErrorException($b, __('Could not create board.'));
        }
        $this->viewBuilder()->setOption('serialize', ['board']);
    }

    /**
     * Delete method
     * POST
     *
     * @return void
     */
    public function delete()
    {
        $this->request->allowMethod(['post']);

        $uid = $this->request->getData('uid');
        if (!$uid) {
            throw new BadRequestException(__('No uid given.'));
        }

        /** @var Board $b */
        $b = $this->Boards->findByUid($uid)->first();
        if (!$b || $b->user_id !== $this->Guardian->user('id')) {
            throw new NotFoundException();
        }

        $res = $this->Boards->delete($b);

        $this->set('response', $res);
        $this->viewBuilder()->setOption('serialize', ['response']);
    }

    /**
     * Retrieve boards list by username.
     * POST
     *
     * @return void
     */
    public function userList()
    {
        $this->request->allowMethod(['post']);

        $username = $this->request->getData('username');
        if (empty($username)) {
            throw new BadRequestException(__('No username provided.'));
        }

        $private = false;
        if ($username === $this->Guardian->user('username')) {
            $private = true;
        }

        $this->set('boards', $this->Boards->findByUsername($username, $private));
        $this->viewBuilder()->setOption('serialize', 'boards');
    }

    /**
     * Retrieve board widgets list by uid.
     * POST
     *
     * @return void
     */
    public function widgetsList()
    {
        $this->request->allowMethod(['post']);

        $uid = $this->request->getData('uid');
        if (!$uid) {
            throw new BadRequestException(__('No uid provided.'));
        }

        /** @var Board $b */
        $b = $this->Boards->findByUid($uid)->contain(['Scripts', 'Users'])->first();
        if (!$b) {
            throw new NotFoundException();
        }

        if ($b->public || (!empty($b->user->username) && ($b->user->username === $this->Guardian->user('username')))) {
            $w = array_map(function ($s) {
                return [
                    'info' => ['script_uid' => $s->uid, 'widget_uid' => $s->_joinData->uid, 'name' => $s->name, 'sandbox' => $s->sandbox, 'dimensions' => $s->dimensions, 'config' => $s->_joinData->config],
                    'layout' => $s->_joinData->layout
                ];
            }, $b->scripts);

            $b = array_filter($b->toArray(), function ($v, $k) {
                return in_array($k, ['uid', 'name', 'description', 'created', 'public', 'config', 'user']) ? [$k => $v] : null;
            }, ARRAY_FILTER_USE_BOTH);
            $b['user'] = array_filter($b['user'], function ($v, $k) {
                return in_array($k, ['username', 'group_id']) ? [$k => $v] : null;
            }, ARRAY_FILTER_USE_BOTH);
            $this->set('board', $b);
            $this->set('widgets', $w);
            $this->viewBuilder()->setOption('serialize', ['board', 'widgets']);
        } else {
            throw new NotFoundException();
        }
    }


    /**
     * @return void
     */
    public function saveEdit()
    {
        $this->request->allowMethod(['post']);

        $data = $this->request->getData();
        if (empty($data['Info']) || empty($data['Info']['uid'])) {
            throw new BadRequestException(__('No uid provided.'));
        }
        /** @var Board $b */
        $b = $this->Boards->findByUid($data['Info']['uid'])->contain(['Users'])->first();
        if (!$b || $b->user->username !== $this->Guardian->user('username')) {
            throw new NotFoundException();
        }

        if (count($data['Info']) > 1) {
            try {
                $success = $this->Boards->saveEdit($b, $data['Info']);
            } catch (\Exception $e) {
                throw new \App\Error\Exception\ValidationErrorException($b, __('Could not update board.'));
            }
            if (!$success) {
                throw new \App\Error\Exception\ValidationErrorException($b, __('Could not update board.'));
            }
        }

        if (!empty($data['Add']) && gettype($data['Add']) === 'array') {
            $ss = $this->Scripts->find()
                ->where(function ($exp, $query) use ($data) {
                    return $exp->and_([
                        $exp->or_([
                            'Scripts.public' => true,
                            'Scripts.user_id' => $this->Guardian->user('id')
                        ]),
                        'Scripts.uid IN' => $data['Add']
                    ]);
                })
                ->toArray();
            $ww = [];
            foreach ($data['Add'] as $sToAdd) {
                $foundS = array_filter($ss, function ($s) use ($sToAdd) {
                    return $s['uid'] === $sToAdd;
                });
                if ($foundS === null) {
                    continue;
                }
                $s = $foundS[0];
                $w = $this->Widgets->create($b, $s);
                $ww[] = $w;
            }

            try {
                $this->Widgets->saveMany($ww);
            } catch (\Exception $ex) {
                throw new InternalErrorException(__('Could not create widget' . count($ww) > 1 ? 's' : '' . '.'));
            }
        }

        if (!empty($data['Delete']) && gettype($data['Delete']) === 'array') {
            // A `deleteAll(...)` would do the job here. I just double check everything to be sure
            $ww = $this->Widgets->find()
                ->where(function ($exp, $query) use ($data) {
                    return $exp->and_([
                        'Widgets.uid IN' => $data['Delete']
                    ]);
                })
                ->matching('Boards', function (Query $q) {
                    return $q->where(['Boards.user_id' => $this->Guardian->user('id')]);
                })
                ->toArray();

            foreach ($ww as $w) {
                if (!empty($w['uid']) && in_array($w['uid'], $data['Delete']) &&
                    !empty($w['_matchingData']['Boards']) && $w['_matchingData']['Boards']['user_id'] === $this->Guardian->user('id')) {
                    $this->Widgets->delete($w);
                }
            }
        }

        if (!empty($data['WidgetsData']) && gettype($data['WidgetsData']) === 'array') {
            try {
                $res = $this->Widgets->saveData($data['WidgetsData'], $this->Guardian->user());
            } catch (\Exception $e) {
                $res = false;
            }
            if (!$res) {
                throw new NotAcceptableException(__('Could not update widgets data.'));
            }
        }

        /*if (!empty($data['layouts']) && gettype($data['layouts']) === 'array') {
            try {
                if (!empty(!$this->Widgets->saveLayouts($data['layouts'], $this->Guardian->user()))) {
                    throw new \Exception();
                }
            } catch (\Exception $e) {
                throw new NotAcceptableException(__('Could not update layout.'));
            }
        }

        if (!empty($data['configs']) && gettype($data['configs']) === 'array') {
            try {
                if (!$this->Widgets->saveConfigs($data['configs'], $this->Guardian->user())) {
                    throw new \Exception();
                }
            } catch (\Exception $e) {
                throw new NotAcceptableException(__('Could not update configs.'));
            }
        }*/

        $this->set('board', $b->uid);
        $this->viewBuilder()->setOption('serialize', ['board', '']);
    }
}
