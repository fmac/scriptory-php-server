<?php

namespace App\Controller\Api;

use App\Error\Exception\ValidationErrorException;
use App\Model\Entity\Token;
use App\Model\Entity\User;
use App\Model\Table\KeypairsTable;
use App\Model\Table\TokensTable;
use App\Model\Table\UsersTable;
use App\Utility\Srp;
use Cake\Core\Configure;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\InternalErrorException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\UnauthorizedException;
use Cake\I18n\Time;
use Cake\Mailer\MailerAwareTrait;

/**
 * Class UserController
 *
 * @property Srp Srp
 * @property UsersTable Users
 * @property TokensTable Tokens
 * @property KeypairsTable Keypairs
 */
class UsersController extends ApiAppController
{
    use MailerAwareTrait;

    /**
     * Initialization hook method.
     *
     * @return void
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadModel('Users');
        $this->loadModel('Tokens');
        $this->loadModel('Keypairs');

        $this->Guardian->allow([
            'loginInfo',
            'auth',
            'usernameAvailable',
            'emailAvailable',
            'signupInfo',
            'signup',
            'verifyEmail',
            'lostPassword',
            'resetPasswordInfo',
            'resetPassword',
            'profile',
            'keypairs',
            'verifyChangeEmail'
        ]);
        $this->Guardian->checkAuthAnyway([
            'profile',
            'boards',
            'scripts',
            'keypairs'
        ]);
    }

    /**
     * Check if username is used.
     * POST
     *
     * @throws \Exception
     * @return void
     */
    public function usernameAvailable()
    {
        $this->request->allowMethod(['post']);

        $this->set('available', $this->Users->usernameAvailable($this->request->getData()['username']));
        $this->viewBuilder()->setOption('serialize', ['available']);
    }

    /**
     * Check if email is used.
     * POST
     *
     * @throws \Exception
     * @return void
     */
    public function emailAvailable()
    {
        $this->request->allowMethod(['post']);

        $this->set('available', $this->Users->emailAvailable($this->request->getData()['email']));
        $this->viewBuilder()->setOption('serialize', ['available']);
    }

    /**
     * SignUp info
     * GET
     *
     * @return void
     * @throws \Exception
     */
    public function signupInfo()
    {
        $this->request->allowMethod(['get']);

        $modulusId = 1;
        $this->set('data', [
            'modulus_uuid' => Configure::read('SRPModulus')[$modulusId]['uuid'],
            'modulus' => base64_encode(strrev(hex2bin(Configure::read('SRPModulus')[$modulusId]['modulus'])))
        ]);

        $this->viewBuilder()->setOption('serialize', ['data']);
    }

    /**
     * Signup action
     * POST
     *
     * @throws \Exception
     * @return void
     */
    public function signup()
    {
        $this->request->allowMethod(['post']);

        $data = $this->request->getData();

        if (count($data['keypairs']) > 1 || isset($data['tokens'])) {
            throw new BadRequestException(__('Invalid keypair or token.'));
        }

        $modulusAvailable = Configure::read('SRPModulus');
        $data['modulus_id'] = array_keys(
            array_combine(
                array_keys($modulusAvailable),
                array_column($modulusAvailable, 'uuid')
            ),
            $data['modulus_uuid']
        )[0];
        $data['created'] = Time::now()->format('Y-m-d H:i:s');
        $data['quota'] = 64;
        $data['secrets'] = '';
        $data['config'] = ['defaultKp' => 1];
        $data['group_id'] = 300;
        $data['keypairs'][0] = array_merge($data['keypairs'][0], [
            'number' => 1,
            'type' => KeypairsTable::TYPE_DEFAULT,
            'purpose' => KeypairsTable::PURPOSE_DEFAULT,
            'valid' => true,
            'created' => $data['created']
        ]);
        $data['tokens'] = [
            [
                'token' => $this->Users->Tokens->generateUniqueToken(),
                'type' => TokensTable::TYPE_VERIFY_EMAIL,
                'expires' => (new \DateTime())->add(new \DateInterval('PT12H')),
                'force_expired' => false,
                'created' => Time::now()->format('Y-m-d H:i:s')
            ]
        ];

        /** @var User $user */
        $user = $this->Users->newEmptyEntity();
        if ($this->Users->create($user, $data, true)) {
            try {
                $this->getMailer('User')->send('signup', [$user, $data['tokens'][0]['token']]);
                $this->set('message', __('Your account has been created.'));
            } catch (\Exception $e) {
                throw new InternalErrorException(__('Mailer not configured.'));
            }
        } else {
            throw new ValidationErrorException($user, __('Please correct the errors.'));
        }

        $this->viewBuilder()->setOption('serialize', ['message', 'errors']);
    }

    /**
     * Retrieve user's salt
     * POST
     *
     * @return void
     * @throws \Exception
     */
    public function loginInfo()
    {
        $this->request->allowMethod(['post']);

        $loginString = $this->request->getData('login_string');
        /** @var User $user */
        $user = $this->Users->find('byUsernameOrEmail', ['user' => $loginString])->first();

        if ($user) {
            if (!$user->verified) {
                throw new UnauthorizedException(__('Please check the verification email to activate your account.'));
            } else {
                $modulus = Configure::read('SRPModulus')[$user->modulus_id]['modulus'];
                $srp = new Srp();
                $srp->prepare(bin2hex(strrev(base64_decode($user->verifier))), $modulus);
                $session = $this->getRequest()->getSession();
                $session->write('Auth.srpSessionId', $srp->sessionId);
                $session->write('Auth.username', $user->username);
                $session->write('Auth.B', $srp->B->toHex());
                $session->write('Auth.b', $srp->b->toHex());

                $this->set('data', [
                    'srp_session' => $srp->sessionId,
                    'modulus' => base64_encode(strrev(hex2bin($modulus))),
                    'salt' => $user->salt,
                    'server_ephemeral' => base64_encode(strrev(hex2bin($srp->Bhex))),
                    'version' => $user->pw_v
                ]);
            }
        } else {
            throw new UnauthorizedException(__('Wrong username/email.'));
        }

        $this->viewBuilder()->setOption('serialize', ['data', 'message']);
    }

    /**
     * Auth action
     * POST
     *
     * @return void
     * @throws \Exception
     */
    public function auth()
    {
        $this->request->allowMethod(['post']);

        $srpSession = $this->request->getData('srp_session');
        $clientEphemeral = $this->request->getData('client_ephemeral');
        $clientProof = $this->request->getData('client_proof');
        $loginString = $this->request->getData('login_string');
        $reqOptions = $this->request->getData('Options');

        /** @var User $user */
        $user = $this->Users->find('byUsernameOrEmail', ['user' => $loginString])->first();
        $session = $this->getRequest()->getSession();
        
        if ($user && $srpSession && $clientEphemeral && $clientProof &&
            ($session->consume('Auth.srpSessionId') === $srpSession) &&
            ($session->consume('Auth.username') === $user->username)) {
            $srp = new Srp();
            if ($srp->verify(
                bin2hex(strrev(base64_decode($clientEphemeral))),
                $session->consume('Auth.B'),
                $session->consume('Auth.b'),
                bin2hex(strrev(base64_decode($user->verifier))),
                Configure::read('SRPModulus')[$user->modulus_id]['modulus'],
                bin2hex(base64_decode($clientProof))
            )
            ) {
                $options = [];

                $this->Guardian->authenticate($user);

                $this->response = $this->response->withHeader(Configure::read('authorizationHeader'), 'Bearer ' . $this->Guardian->user('accessToken.token'));

                if (isset($reqOptions['ChangePassword'])) {
                    $this->Tokens->expireTokens($user->id, TokensTable::TYPE_CHANGE_PASSWORD);
                    $this->Tokens->create($user, TokensTable::TYPE_CHANGE_PASSWORD, 'PT5M');
                    /** @var Token $tok */
                    $tok = $this->Tokens->find()
                        ->where(['user_id' => $user->id, 'type' => TokensTable::TYPE_CHANGE_PASSWORD, 'force_expired' => false])
                        ->first();
                    $options['PasswordChangeAuth'] = $tok->token;
                }

                $this->set('data', [
                    'username' => $user->username,
                    'server_proof' => base64_encode($srp->serverProof),
                    'Options' => $options
                ]);
            } else {
                throw new UnauthorizedException(__('Authentication failed.'));
            }
        } else {
            throw new UnauthorizedException(__('Wrong SRP session data.'));
        }

        $this->viewBuilder()->setOption('serialize', ['data', 'message']);
    }

    /**
     * Logout action
     * POST
     *
     * @return void
     */
    public function logout()
    {
        $this->request->allowMethod(['post']);

        $this->Guardian->logout();
        $this->set('message', __('You were logged out.'));
        $this->viewBuilder()->setOption('serialize', ['message']);
    }

    /**
     * Retrieve authenticated user info.
     * GET
     *
     * @return void
     */
    public function info()
    {
        $this->request->allowMethod(['get']);

        $u = $this->Guardian->user();
        if (!$u) {
            throw new UnauthorizedException();
        }

        // id omitted. We don't want users ids to leave the server
        $this->set('user', [
            'username' => $u->username,
            'email' => $u->email,
            'verified' => $u->verified,
            'secrets' => $u->secrets,
            'config' => $u->config,
            'quota' => $u->quota * 1048576,
            //'quota_left' => $this->Users->quotaLeft($u),
            'group_id' => $u->group_id
        ]);
        $this->viewBuilder()->setOption('serialize', ['user']);
    }

    /**
     * Retrieve keypairs.
     * POST
     *
     * @return void
     */
    public function keypairs()
    {
        $this->request->allowMethod(['post']);

        $username = $this->request->getData('username');
        $widgets = $this->request->getData('widgets');
        if (empty($username)) {
            throw new BadRequestException(__('No user given.'));
        }
        $private = $username === $this->Guardian->user('username');

        $kk = $this->Keypairs->find('byUsername', ['username' => $username]);
        if (!empty($widgets)) {
            $kk = $kk->find('byWidgets', ['widgets' => $widgets]);
        }
        $kk = $kk->select(['Keypairs.type', 'Keypairs.purpose', 'Keypairs.valid', 'Keypairs.pub_key']);
        if ($private) {
            $kk = $kk->select(['Keypairs.number', 'Keypairs.rev_cert', 'Keypairs.enc_priv_key', 'Keypairs.salt', 'Keypairs.created']);
        }
        $kk = $kk->toArray();
        if (empty($kk)) {
            throw new NotFoundException(__('No keypair found.'));
        }

        $this->set('keypairs', $kk);
        $this->viewBuilder()->setOption('serialize', ['keypairs']);
    }

    /**
     * @return void
     */
    public function verifyEmail()
    {
        $this->request->allowMethod(['post']);

        $token = $this->request->getData('token');
        if (empty($token)) {
            throw new BadRequestException(__('No token provided.'));
        }

        $user = $this->Users->find('byToken', ['token' => $token, 'type' => TokensTable::TYPE_VERIFY_EMAIL])->first();
        if (empty($user)) {
            throw new BadRequestException(__('This verification token is no longer valid.'));
        }

        $this->Users->verify($user->id);
        $this->Users->Tokens->expireToken($token);

        $this->set('message', __('Your email address has been verified.'));
        $this->viewBuilder()->setOption('serialize', ['message']);
    }

    /**
     * Lost password action
     * POST
     *
     * @return void
     * @throws \Exception
     */
    public function lostPassword()
    {
        $this->request->allowMethod(['post']);

        /** @var User $user */
        $user = $this->Users->find('byUsernameOrEmail', ['user' => $this->request->getData('LoginString')])->first();

        if ($user) {
            $token = $this->Tokens->create($user, TokensTable::TYPE_RESET_PASSWORD);
            try {
                $this->getMailer('User')->send('lostPassword', [$user, $token->token]);
                $message = __('Go check your mail!');
            } catch (\Exception $e) {
                throw new BadRequestException(__('Mailer not configured.'));
            }
        } else {
            throw new NotFoundException(__('User not found.'));
        }

        $this->set('message', $message);
        $this->viewBuilder()->setOption('serialize', 'message');
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function resetPasswordInfo()
    {
        $this->request->allowMethod(['post']);

        $token = $this->request->getData('Token');
        if (empty($token)) {
            throw new BadRequestException(__('No token given.'));
        }

        /** @var User $user */
        $user = $this->Users->find('byToken', ['token' => $token, 'type' => TokensTable::TYPE_RESET_PASSWORD])->first();
        if (empty($user)) {
            throw new BadRequestException(__('This password reset token is no longer valid.'));
        }

        $modulusId = $user->modulus_id;

        $res = [
            'Username' => $user->username,
            'ModulusUUID' => Configure::read('SRPModulus')[$modulusId]['uuid'],
            'Modulus' => base64_encode(strrev(hex2bin(Configure::read('SRPModulus')[$modulusId]['modulus']))),
            'Salt' => $user->salt
        ];
        $this->set($res);
        $this->viewBuilder()->setOption('serialize', array_keys($res));
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function resetPassword()
    {
        $this->request->allowMethod(['post']);

        $data = $this->request->getData();
        if (empty($data['Token']) ||
            empty($data['Username']) ||
            empty($data['PwV']) ||
            empty($data['Salt']) ||
            empty($data['ModulusUUID']) ||
            empty($data['Verifier']) ||
            empty($data['Keypairs']) ||
            count($data['Keypairs']) > 1) {
            throw new BadRequestException(__('Invalid data given.'));
        }

        $token = $data['Token'];
        $user = $this->Users->find('byToken', ['token' => $token, 'type' => TokensTable::TYPE_RESET_PASSWORD])
            ->find('byUsernameOrEmail', ['user' => $data['Username']])
            ->first();
        if (empty($user)) {
            throw new BadRequestException(__('This reset token is no longer valid.'));
        }

        $modulusAvailable = Configure::read('SRPModulus');
        $modulusId = array_keys(
            array_combine(
                array_keys($modulusAvailable),
                array_column($modulusAvailable, 'uuid')
            ),
            $data['ModulusUUID']
        )[0];

        $allKp = $this->Keypairs->find()
            ->where(['Keypairs.user_id' => $user->id])
            ->toArray();
        $number = array_reduce(
            $allKp ? $allKp : [],
            function ($carry, $k) {
                return $k->number >= $carry ? $k->number + 1 : $carry;
            },
            1
        );
        $keypair = array_merge($data['Keypairs'][0], [
            'number' => $number,
            'type' => KeypairsTable::TYPE_DEFAULT,
            'purpose' => KeypairsTable::PURPOSE_DEFAULT,
            'valid' => true,
            'created' => Time::now()->format('Y-m-d H:i:s')
        ]);

        $user = $this->Users->patchEntity($user, [
            'pw_v' => $data['PwV'],
            'salt' => $data['Salt'],
            'modulus_id' => $modulusId,
            'verifier' => $data['Verifier'],
            'config' => array_merge((array)$user->config ?? [], ['defaultKp' => $number]),
            'secrets' => null,
            'keypairs' => [$keypair]
        ], [
            'validate' => 'resetPassword',
            'fieldList' => ['pw_v', 'salt', 'modulus_id', 'verifier', 'config', 'secrets', 'keypairs'],
            'associated' => [
                'Keypairs' => [
                    'validate' => 'creation',
                    'fieldList' => ['number', 'type', 'purpose', 'valid', 'pub_key', 'rev_cert', 'enc_priv_key', 'salt', 'created']
                ]
            ]
        ]);

        if ($this->Users->save($user)) {
            $this->Users->Tokens->expireTokens($user->id, TokensTable::TYPE_RESET_PASSWORD);
            $this->Guardian->expireAllSessions($user);
            $this->set('message', __('Your password has been reset.'));
        } else {
            throw new ValidationErrorException($user, __('Could not update user info.'));
        }

        $this->viewBuilder()->setOption('serialize', ['message']);
    }

    /**
     * Retrieve profile data.
     * POST
     *
     * @return void
     */
    public function profile()
    {
        $this->request->allowMethod(['post']);

        $username = $this->request->getData('username');
        if (empty($username)) {
            throw new BadRequestException(__('No username given.'));
        }

        $private = false;
        if ($username === $this->Guardian->user('username')) {
            $private = true;
        }

        $data = $this->Users->profile($username, $private);
        if (!$data) {
            throw new NotFoundException(__('User not found.'));
        }

        $this->set('profile', $data);
        $this->viewBuilder()->setOption('serialize', 'profile');
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function changeEmail()
    {
        $this->request->allowMethod(['post']);

        $newEmail = $this->request->getData('NewEmail');
        if (empty($newEmail) || !(bool)preg_match(UsersTable::$validEmailRegEx, $newEmail)) {
            throw new BadRequestException(__('No email address or invalid address given.'));
        }
        if ($this->Users->findByEmail($newEmail)->first()) {
            throw new BadRequestException(__('Email already in use.'));
        }

        $u = $this->Users->get($this->Guardian->user('id'));
        $token = $this->Users->Tokens->generateUniqueToken();

        $this->Users->patchEntity($u, ['tokens' => [
            [
                'token' => $token,
                'type' => TokensTable::TYPE_CHANGE_EMAIL,
                'expires' => (new \DateTime())->add(new \DateInterval('PT12H')),
                'force_expired' => false,
                'created' => Time::now()->format('Y-m-d H:i:s'),
                'info' => ['newEmail' => $newEmail]
            ]
        ]], ['associated' => ['Tokens']]);

        if (!$this->Users->save($u)) {
            throw new InternalErrorException(__('Could not create token.'));
        }

        try {
            $this->getMailer('User')->send('verifyChangeEmail', [$u, $token, $newEmail]);
        } catch (\Exception $e) {
            throw new InternalErrorException(__('Mailer not configured.'));
        }

        $this->set('response', true);
        $this->viewBuilder()->setOption('serialize', ['response']);
    }

    /**
     * @return void
     */
    public function verifyChangeEmail()
    {
        $this->request->allowMethod(['post']);

        $token = $this->request->getData('Token');
        if (empty($token)) {
            throw new BadRequestException(__('No token given.'));
        }

        $u = $this->Users->find('byToken', ['token' => $token, 'type' => TokensTable::TYPE_CHANGE_EMAIL])->first();
        if (empty($u)) {
            throw new BadRequestException(__('This token is no longer valid.'));
        }

        $email = $u->_matchingData['Tokens']->info['newEmail'];
        if (!(bool)preg_match(UsersTable::$validEmailRegEx, $email)) {
            throw new InternalErrorException(__('Could not update email.'));
        }

        $this->Users->patchEntity($u, ['email' => $email]);
        if (!$this->Users->save($u)) {
            throw new InternalErrorException(__('Could not update email.'));
        }

        $this->Tokens->expireTokens($u->id, TokensTable::TYPE_CHANGE_EMAIL);

        $this->set('response', true);
        $this->viewBuilder()->setOption('serialize', ['response']);
    }

    /**
     * @return void
     */
    public function changePassword()
    {
        $this->request->allowMethod(['post']);

        $token = $this->request->getData('ChangePasswordAuth');
        $verifier = $this->request->getData('Verifier');
        $salt = $this->request->getData('Salt');
        $keypairs = $this->request->getData('Keypairs');
        if (empty($token) || empty($verifier) || empty($salt)) {
            throw new BadRequestException(__('Invalid data given.'));
        }

        $u = $this->Users->find('byToken', ['token' => $token, 'type' => TokensTable::TYPE_CHANGE_PASSWORD])->first();
        if (!$u || $u->id !== $this->Guardian->user('id')) {
            throw new BadRequestException(__('Invalid change-password-auth token.'));
        }

        try {
            $result = $this->Users->getConnection()->transactional(function () use ($u, $verifier, $salt, $keypairs) {
                $u->verifier = $verifier;
                $u->salt = $salt;
                $this->Users->save($u, ['atomic' => false]);
                foreach ($keypairs as $k) {
                    if (!$k) {
                        continue;
                    }
                    $dbK = $this->Keypairs->find()
                        ->where(['Keypairs.number' => $k['number'], 'Keypairs.user_id' => $u->id])
                        ->first();
                    if ($dbK) {
                        $dbK->enc_priv_key = $k['enc_priv_key'];
                        $this->Keypairs->save($dbK, ['atomic' => false]);
                    }
                }
            });

            $this->Guardian->expireAllSessionsButThis();

            $this->set('Response', true);
        } catch (\Exception $ex) {
            $result = false;
        }

        $this->Tokens->expireTokens($u->id, TokensTable::TYPE_CHANGE_PASSWORD);

        if ($result === false) {
            throw new InternalErrorException(__('Could not update user data.'));
        }

        $this->set('Response', true);
        $this->viewBuilder()->setOption('serialize', ['Response']);
    }

    /**
     * @return void
     */
    public function updateSecrets()
    {
        $this->request->allowMethod(['post']);

        $sec = $this->request->getData('Secrets');
        if (!$sec) {
            throw new BadRequestException(__('No secrets string given.'));
        }
        $u = $this->Guardian->user();
        $this->Users->patchEntity($u, ['secrets' => $sec], ['validate' => 'update', 'fieldList' => ['secrets']]);
        if (!$this->Users->save($u)) {
            throw new ValidationErrorException($u, __('Could not update user\'s secrets.'));
        }

        $this->set('Response', true);
        $this->viewBuilder()->setOption('serialize', ['Response']);
    }

    /**
     * @return void
     */
    public function updateConfig()
    {
        $this->request->allowMethod(['post']);

        $conf = $this->request->getData('Config');
        if (!$conf) {
            throw new BadRequestException(__('No secrets string given.'));
        }
        $u = $this->Guardian->user();
        $this->Users->patchEntity($u, ['config' => $conf], ['validate' => 'update', 'fieldList' => ['config']]);
        if (!$this->Users->save($u)) {
            throw new ValidationErrorException($u, __('Could not update user\'s config.'));
        }

        $this->set('Response', true);
        $this->viewBuilder()->setOption('serialize', ['Response']);
    }
}
