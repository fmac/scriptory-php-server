<?php

namespace App\Controller\Api;

use Cake\Http\Exception\NotFoundException;

/**
 * Class DocsController
 */
class DocsController extends ApiAppController
{
    /**
     * @return void
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->Guardian->allow([
            'fetch'
        ]);
    }

    /**
     * GET
     *
     * @param string $name The doc name to fetch
     * @return \Cake\Http\Response
     */
    public function fetch($name)
    {
        $this->request->allowMethod(['get']);

        $this->response = $this->response->withType('txt');

        if (!$name || !(bool)preg_match('/^[a-zA-Z0-9](-(?!(\.|-))|\.(?!(-|\.))|[a-zA-Z0-9]){1,512}[a-zA-Z0-9]$/', $name)) {
            throw new NotFoundException();
        }

        return $this->response = $this->response->withFile(WWW_ROOT . 'docsmd' . DS . $name . '.md', ['download' => false, 'name' => $name]);
    }
}
