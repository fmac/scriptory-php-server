<?php
namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Http\Exception\BadRequestException;

/**
 * Class ApiAppController
 *
 * @property GuardianComponent Guardian
 */
class ApiAppController extends AppController
{
    /**
     * @return void
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->RequestHandler->renderAs($this, 'json');
        $this->response = $this->response->withType('application/json');

        $this->loadComponent('Guardian');
    }

    /**
     * - ensure the incoming request is an ajax request
     * - check if a valid user token is provided
     * - if not log out the user
     *
     * @param Event $event
     * @return \Cake\Http\Response|null|void
     */
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);

        if (!$this->request->is('ajax')) {
            throw new BadRequestException();
        }
    }

    /**
     * Respond with the given status $code and the specified $reason.
     *
     * @param int $code
     * @param string $reason
     * @return \Cake\Http\Response
     */
    /*protected function _respondWith($code = 200, $reason = '')
        {
            return $this->response = $this->response->withStatus($code, $reason);
        }
    
        protected function _respondWithBadRequest()
        {
            return $this->_respondWith(400, 'BAD REQUEST');
        }
    
        protected function _respondWithUnauthorized()
        {
            return $this->_respondWith(401, 'UNAUTHORIZED');
        }
    
        protected function _respondWithNotFound()
        {
            return $this->_respondWith(404, 'NOT FOUND');
        }
    
        protected function _respondWithMethodNotAllowed()
        {
            return $this->_respondWith(405, 'METHOD NOT ALLOWED');
        }
    
        protected function _respondWithValidationErrors()
        {
            return $this->_respondWith(422, 'UNPROCESSABLE ENTITY');
        }
    
        protected function _respondWithConflict()
        {
            return $this->_respondWith(409, 'CONFLICT');
        }*/
    
    /**
     * NotFound action
     * ANY
     *
     * Default api action that is called when no other api route matched.
     *
     * @return void
     */
    public function notFound()
    {
        $this->set('message', 'API endpoint "' . $this->request->getUri()->getPath() . '" does not exist.');
        //$this->set('_serialize', ['message']);
        \Cake\View\ViewBuilder::setOption('_serialize', ['message']);
        $this->response = $this->response->withStatus(404, 'NOT FOUND');
    }
}
