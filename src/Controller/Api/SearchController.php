<?php

namespace App\Controller\Api;

use App\Model\Table\UsersTable;
use Cake\Database\Expression\FunctionExpression;
use Cake\Database\Expression\QueryExpression;

/**
 * Class SearchController
 *
 * @property UsersTable Users
 * @property ScriptsTable Scripts
 * @property BoardsTable Boards
 * @property TagsTable Tags
 */
class SearchController extends ApiAppController
{
    /**
     * @return void
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadModel('Users');
        $this->loadModel('Scripts');
        $this->loadModel('Boards');
        $this->loadModel('Tags');

        $this->Guardian->allow([
            'search'
        ]);
        $this->Guardian->checkAuthAnyway([
            'search'
        ]);
    }

    /**
     * POST
     *
     * @return void
     */
    public function search()
    {
        $this->request->allowMethod(['post']);
        
        $sString = $this->request->getData('searchString') ?? '';
        $cats = $this->request->getData('categories') ?? ['scripts'];
        $order = $this->request->getData('order') ?? 1;
        $page = $this->request->getData('page') ?? 1;
        $options = $this->request->getData('options') ?? [];

        $results = [];
        $orderBy = [];

        /*if (in_array('users', $cats)) {
            $results['users'] = TableRegistry::getTableLocator()->get('Users')->find()
                ->select(['Users.uid', 'Users.created', 'Users.group_id'])
                ->where(function (QueryExpression $exp) use ($sString) {
                    return $exp->between((new FunctionExpression('levenshtein', [strtolower($sString), 'LOWER(username)' => 'literal'])), 0, 4);
                })
                ->limit(20)
                ->page($page)
                ->toArray();
        }*/

        if (in_array('scripts', $cats)) {
            $where = function (QueryExpression $exp, $query) use ($sString, $options) {
                $pubCond = ($options['private'] ?? true) ?
                    $exp->or_([
                        'Scripts.public' => true,
                        'Users.username' => $this->Guardian->user('username') ?? ''
                    ]) :
                    ['Scripts.public' => true];

                return $exp->and_([
                    $pubCond,
                    'Scripts.uid NOT IN' => ['sboardctl'],
                    $exp->or_([
                        function (QueryExpression $exp) use ($sString) {
                            return $exp->between((new FunctionExpression('levenshtein', [strtolower($sString), 'LOWER(Scripts.name)' => 'literal'])), 0, 3);
                        },
                        'MATCH (Scripts.name) AGAINST (:sString)',
                        'Scripts.id IN' => $this->Tags->find()->select(['Scripts.id'])->where(['Tags.text IN' => explode(' ', $sString)])->matching('Scripts'),
                        'REPLACE(Scripts.name, \' \', \'\') LIKE' => '%' . str_replace(' ', '', $sString) . '%'
                    ])
                ]);
            };

            $results['scripts'] = $this->Scripts->find()
                ->select(['Scripts.uid', 'Scripts.name', 'Users.username', 'Scripts.public', 'name_score' => 'MATCH (Scripts.name) AGAINST (:sString)'])
                ->where($where)
                ->contain(['Users'])
                ->order(['name_score' => 'DESC', 'modified' => 'DESC'])
                ->limit(20)
                ->page($page)
                ->bind(':sString', $sString, 'string')
                ->toArray();

            foreach ($results['scripts'] as $s) {
                $s['user']['flag'] = UsersTable::flag($s['user']['username']);
                // I didn't get how to tell the ORM to automatically find the tags
                $s['tags'] = $this->Tags->find()->select(['Tags.text'])->where(['Scripts.uid' => $s['uid']])->matching('Scripts');
            }
        }

        if (in_array('boards', $cats)) {
            $where = function (QueryExpression $exp, $query) use ($sString, $options) {
                $pubCond = ($options['private'] ?? true) ?
                    $exp->or_([
                        'Boards.public' => true,
                        'Users.username' => $this->Guardian->user('username') ?? ''
                    ]) :
                    ['Boards.public' => true];

                return $exp->and_([
                    $pubCond,
                    $exp->or_([
                        function (QueryExpression $exp) use ($sString) {
                            return $exp->between((new FunctionExpression('levenshtein', [strtolower($sString), 'LOWER(Boards.name)' => 'literal'])), 0, 3);
                        },
                        'MATCH (Boards.name) AGAINST (:sString)',
                        'REPLACE(Boards.name, \' \', \'\') LIKE' => '%' . str_replace(' ', '', $sString) . '%'
                    ])
                ]);
            };

            switch ($order) {
                case 'recent':
                    $orderBy = ['Boards.created' => 'DESC'];
                    break;
                default:
                    break;
            }
            $orderBy += ['name_score' => 'DESC', 'Boards.modified' => 'DESC'];

            $results['boards'] = $this->Boards->find()
                ->select(['Boards.uid', 'Boards.name', 'Boards.description', 'Boards.public', 'Users.username', 'name_score' => 'MATCH (Boards.name) AGAINST (:sString)'])
                ->where($where)
                ->contain(['Users'])
                ->order($orderBy)
                ->limit(20)
                ->page($page)
                ->bind(':sString', $sString, 'string')
                ->toArray();

            foreach ($results['boards'] as $b) {
                $b['user']['flag'] = UsersTable::flag($b['user']['username']);
            }
        }

        $this->set(compact('results'));
        //$this->set('_serialize', ['results']);
        //$this->viewBuilder()->setOption('results', compact('results'));
        $this->viewBuilder()->setOption('serialize', ['results']);
    }
}
