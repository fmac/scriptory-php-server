<?php

namespace App\Controller\Api;

use App\Model\Entity\Script;
use App\Model\Table\UsersTable;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\InternalErrorException;
use Cake\Http\Exception\NotFoundException;

/**
 * Class ScriptsController
 *
 * @property UsersTable Users
 * @property ScriptsTable Scripts
 * @property TagsTable Tags
 */
class ScriptsController extends ApiAppController
{
    /**
     * Initialization hook method.
     *
     * @return void
     * @throws \Exception
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadModel('Users');
        $this->loadModel('Scripts');
        $this->loadModel('Tags');

        $this->Guardian->allow([
            'getHtml',
            'getReference',
            'getScriptAsWidget',
            'newAndPopular',
            'userList',
            'sitemap'
        ]);
        $this->Guardian->checkAuthAnyway([
            'getHtml',
            'getReference',
            'getScriptAsWidget',
            'userList'
        ]);
    }

    /**
     * Retrieve scripts list by username.
     * POST
     *
     * @return void
     */
    public function userList()
    {
        $this->request->allowMethod(['post']);

        $username = $this->request->getData('username');
        if (empty($username)) {
            throw new BadRequestException(__('No username provided'));
        }

        $private = false;
        if ($username === $this->Guardian->user('username')) {
            $private = true;
        }

        $this->set('scripts', $this->Scripts->findByUsername($username, $private));
        $this->viewBuilder()->setOption('serialize', 'scripts');
    }

    /**
     * Delete script by uid.
     * POST
     *
     * @return void
     */
    public function deleteScript()
    {
        $this->request->allowMethod(['post']);

        $uid = $this->request->getData('uid');
        if (empty($uid)) {
            throw new BadRequestException();
        }

        if ($this->Scripts->deleteByUid($uid)) {
            $this->set('success', true);
        } else {
            throw new InternalErrorException();
        }

        $this->viewBuilder()->setOption('serialize', 'success');
    }

    /**
     * Retrieve script data for edit or create a new one if empty ref is given.
     * POST
     *
     * @return void
     * @throws \Exception
     */
    public function getEdit()
    {
        $this->request->allowMethod(['post']);

        $uid = $this->request->getData('uid');
        /** @var Script $s */
        $s = $this->Scripts->findByUid($uid)->contain(['Users', 'Tags'])->first();

        if ($s) {
            if ($s->user->username === $this->Guardian->user('username')) {
                $this->Scripts->appendTagsString($s);
                $this->Scripts->appendFiles($s, ['desc', 'code']);
                $this->set('data', $s);
            } else {
                throw new NotFoundException();
            }
        } else {
            $s = $this->Scripts->newEmptyEntity();
            $this->Scripts->create($s, $this->Guardian->user());
            $s = $this->Scripts->findByUid($s->uid)->first();
            $this->set('data', $s);
        }

        $this->viewBuilder()->setOption('serialize', ['data']);
    }

    /**
     * Save script data after edit.
     * POST
     *
     * @return void
     * @throws \Exception
     */
    public function saveEdit()
    {
        $this->request->allowMethod(['post']);

        $data = $this->request->getData('data');
        /** @var Script $s */
        $s = $this->Scripts->findByUid($data['uid'])->contain(['Users'])->first();
        if (!$s) {
            throw new NotFoundException();
        }

        if ($s->user->username === $this->Guardian->user('username')) {
            if ($this->Scripts->saveEdit($s, $this->Guardian->user(), $data)) {
                $this->set('message', $s->uid);
            } else {
                throw new \App\Error\Exception\ValidationErrorException($s, __('Could not save script data'));
            }
        } else {
            throw new NotFoundException();
        }

        $this->viewBuilder()->setOption('serialize', ['message']);
    }

    /**
     * Save script data after edit.
     * POST
     *
     * @return void
     * @throws \Exception
     */
    public function clone()
    {
        $this->request->allowMethod(['post']);

        $uid = $this->request->getData('uid');
        /** @var Script $s */
        $s = $this->Scripts->findByUid($uid)->contain(['Users'])->first();
        if (!$s || (!$s->public && $s->user->username !== $this->Guardian->user('username'))) {
            throw new NotFoundException();
        }

        try {
            $this->Scripts->clone($s, $this->Guardian->user());
            $response = ['Code' => 1000, 'Message' => 'Cloned!'];
        } catch (\Exception $ex) {
            $response = ['Code' => 2000, 'Message' => 'Could not clone script.' . $ex->getMessage() ?? ''];
        }

        $this->set('response', $response);
        $this->viewBuilder()->setOption('serialize', 'response');
    }

    /**
     * Retrieve script for test by uid.
     * POST
     *
     * @return void
     */
    public function getScriptAsWidget()
    {
        $this->request->allowMethod(['post']);

        $uid = $this->request->getData('uid');
        if (!$uid) {
            throw new BadRequestException(__('No uid provided'));
        }

        /** @var Script $s */
        $s = $this->Scripts->findByUid($uid)->contain(['Users'])->first();
        if (!$s) {
            throw new NotFoundException();
        }

        if ($s->public || (!empty($s->user->username) && ($s->user->username === $this->Guardian->user('username')))) {
            $res = [
                'info' => ['script_uid' => $s->uid, 'widget_uid' => $s->uid . '_' . substr(md5(uniqid()), -3), 'name' => $s->name, 'sandbox' => $s->sandbox, 'dimensions' => $s->dimensions, 'config' => (object)[]],
                'layout' => array_merge((array)$s->dimensions, ['x' => 0/*6 - ceil(intval($s->dimensions->w) / 2)*/, 'y' => 0])
            ];
            $this->set('widget', $res);
            $this->viewBuilder()->setOption('serialize', ['widget']);
        } else {
            throw new NotFoundException();
        }
    }

    /**
     * Respond with script file as html page.
     * POST
     *
     * @return \Cake\Http\Response
     */
    public function getHtml()
    {
        $this->request->allowMethod(['post']);

        $this->response = $this->response->withType('html');

        $uid = $this->request->getData('uid');
        $type = $this->request->getData('type');
        if (!$uid || !$type) {
            throw new BadRequestException(__('No uid or type provided'));
        }

        /** @var Script $s */
        $s = $this->Scripts->findByUid($uid)->contain(['Users'])->first();
        if (!$s) {
            throw new NotFoundException();
        }

        $path = $this->Scripts->scriptFilePath($s, $type);
        if (!$path) {
            throw new NotFoundException();
        }

        if ($s->public || (!empty($s->user->username) && ($s->user->username === $this->Guardian->user('username')))) {
            return $this->response = $this->response->withFile($path, ['download' => false, 'name' => $uid]);
        }

        throw new NotFoundException();
    }

    /**
     * Retrieve script data and description file by uid.
     * POST
     *
     * @return void
     */
    public function getReference()
    {
        $this->request->allowMethod(['post']);

        $uid = $this->request->getData('uid');
        if (!$uid) {
            throw new BadRequestException(__('No uid provided'));
        }

        /** @var Script $s */
        $s = $this->Scripts->findByUid($uid)->select($this->Scripts)->select($this->Users)->find('popularity', ['uid' => $uid])->contain(['Users', 'Tags'])->first();
        if (!$s) {
            throw new NotFoundException();
        }
        if (!$s->public && $s->user->username !== $this->Guardian->user('username')) {
            throw new NotFoundException();
        }
        $this->Scripts->parseTagsString($s);
        $this->Scripts->appendFiles($s, ['desc', 'code']);

        $s->user->flag = UsersTable::flag($s->user->username);
        $s = array_filter($s->toArray(), function ($v, $k) {
            return in_array($k, ['uid', 'created', 'name', 'public', 'sandbox', 'popularity', 'user', 'tags', 'desc', 'code']) ? [$k => $v] : null;
        }, ARRAY_FILTER_USE_BOTH);
        $s['user'] = array_filter($s['user'], function ($v, $k) {
            return in_array($k, ['username', 'flag']) ? [$k => $v] : null;
        }, ARRAY_FILTER_USE_BOTH);
        $this->set('script', $s);
        $this->viewBuilder()->setOption('serialize', ['script']);
    }

    /**
     * GET
     *
     * @return void
     */
    public function newAndPopular()
    {
        $this->request->allowMethod(['get']);

        $list = $this->Scripts->findNewAndPopular(6);
        if (!$list) {
            throw new NotFoundException();
        } else {
            $this->set('list', $list);
        }

        $this->viewBuilder()->setOption('serialize', ['list']);
    }
}
