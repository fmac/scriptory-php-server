<?php

namespace App\Utility;

use phpseclib\Crypt\Random;
use phpseclib\Math\BigInteger;

class Srp
{
    public $sessionId;

    /** @var BigInteger Password verifier */
    public $verifier;

    /** @var BigInteger|string */
    public $N;
    public $g;
    public $k;
    public $v;
    public $A;
    public $Ahex;

    /** @var BigInteger|null Secure Random Number */
    public $b = null;

    /** @var BigInteger|null */
    public $B = null;
    public $Bhex;

    public $expectedClientProof;
    public $serverProof;

    public $key;

    /**
     * Srp constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $string
     * @return array
     */
    public static function string2ByteArray($string)
    {
        return unpack('C*', $string);
    }

    /**
     * @param $byteArray
     * @return string
     */
    public static function byteArray2String($byteArray)
    {
        $chars = array_map("chr", $byteArray);

        return join($chars);
    }

    /**
     * @param $byteArray
     * @return string
     */
    public static function byteArray2Hex($byteArray)
    {
        $chars = array_map("chr", $byteArray);
        $bin = join($chars);

        return bin2hex($bin);
    }

    /**
     * @param $hexString
     * @return array
     */
    public static function hex2ByteArray($hexString)
    {
        $string = hex2bin($hexString);

        return unpack('C*', $string);
    }

    /**
     * @return BigInteger 0 as BigInteger
     */
    protected static function zeroBN()
    {
        return new BigInteger('0', 16);
    }

    /**
     * @return BigInteger 1 as BigInteger
     */
    protected static function oneBN()
    {
        return new BigInteger('1', 16);
    }

    /**
     * @return BigInteger 2 as BigInteger
     */
    protected static function twoBN()
    {
        return new BigInteger('2', 16);
    }

    /**
     * @param int $x
     * @param int $base
     * @return BigInteger
     */
    public static function getBigInteger($x = 0, $base = 16)
    {
        return new BigInteger($x, $base);
    }

    /**
     * @param $x
     * @return string
     */
    public function expandHash($x)
    {
        return strtolower(hash('sha512', $x . "\x00") . hash('sha512', $x . "\x01") . hash('sha512', $x . "\x02") . hash('sha512', $x . "\x03"));
    }

    /**
     * @param $x
     * @return string
     */
    public function srpHasher($x)
    {
        return $this->expandHash($x);
    }

    /**
     * @return string
     */
    public static function generateSessionId()
    {
        return bin2hex(Random::string(32));
    }

    /**
     * @param $verifier
     * @param $modulus
     * @return void
     */
    public function prepare($verifier, $modulus)
    {
        $this->sessionId = self::generateSessionId();
        $this->verifier = $verifier;
        $this->N = $this->getBigInteger($modulus, 16);
        $this->g = self::twoBN();
        $this->k = $this->getBigInteger(bin2hex(strrev(hex2bin($this->srpHasher(str_pad(hex2bin($this->g->toHex()), 256, hex2bin('00')) . strrev(hex2bin($this->N->toHex())))))), 16);

        $this->v = $this->getBigInteger($verifier, 16);

        while (!$this->B || bcmod($this->B, $this->N) == 0) {
            $this->b = $this->getBigInteger(bin2hex(Random::string(64)), 16);
            $gPowed = $this->g->powMod($this->b, $this->N);
            $this->B = $this->k->multiply($this->v)->add($gPowed)->powMod(self::oneBN(), $this->N);
        }

        $this->Bhex = $this->B->toHex();
    }

    /**
     * @param string $clientEphemeral
     * @param string $serverPubEphemeral
     * @param string $serverPrivEphemeral
     * @param string $verifier
     * @param string $modulus
     * @param string $clientProof
     * @return bool
     */
    public function verify($clientEphemeral, $serverPubEphemeral, $serverPrivEphemeral, $verifier, $modulus, $clientProof)
    {
        $this->B = $this->getBigInteger($serverPubEphemeral, 16);
        $this->Bhex = $this->B->toHex();
        $this->b = $this->getBigInteger($serverPrivEphemeral, 16);
        $this->A = $this->getBigInteger($clientEphemeral, 16);
        $this->Ahex = $this->A->toHex();

        $this->N = $this->getBigInteger($modulus, 16);

        if ($this->A->powMod(self::oneBN(), $this->N) === 0) {
            return false; // Client sent invalid key: A mod N == 0.
        }

        $u = $this->getBigInteger(bin2hex(strrev(hex2bin($this->srpHasher(strrev(hex2bin($this->Ahex)) . strrev(hex2bin($this->Bhex)))))), 16);
        $v = $this->getBigInteger($verifier, 16);
        $avu = $this->A->multiply($v->powMod($u, $this->N));

        $S = $avu->modPow($this->b, $this->N);
        $this->key = $S->toBytes();

        $this->expectedClientProof = $this->srpHasher(strrev(hex2bin($this->Ahex)) . strrev(hex2bin($this->Bhex)) . strrev($this->key));
        $this->serverProof = $this->srpHasher(strrev(hex2bin($this->Ahex)) . hex2bin($this->expectedClientProof) . strrev($this->key));

        if ($this->expectedClientProof === $clientProof) {
            return true;
        }

        return false;
    }
}
