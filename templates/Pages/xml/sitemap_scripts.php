<?= '<?xml version="1.0" encoding="UTF-8"?>' ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <?php foreach ($scripts as $s): ?>
    <?php
    $name = $safe = preg_replace('/^-+|-+$/', '', strtolower(preg_replace('/[^a-zA-Z0-9]+/', '-', $s->name)));
    $modified = '1970.01.01';
    try {
        $modified = (new DateTime($s->modified))->format('Y-m-d');
    } catch (\Exception $e) {
        $modified = '1970.01.01';
    }
    ?>

    <url>

        <loc>https://scriptory.io/s/view/<?= $name ?>/<?= $s->uid ?></loc>

        <lastmod><?= $modified ?></lastmod>

        <changefreq>monthly</changefreq>

        <priority>0.8</priority>

    </url>

    <?php endforeach; ?>
</urlset>
