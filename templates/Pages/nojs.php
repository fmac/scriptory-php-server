<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
    <head>
        <title>Scriptory</title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
	        body {
		        background-color: #121619;
		        color: #F1E4E8;
		        font-family: "Avenir Next", sans-serif;
	        }
            .container {
                width: 300px;
                padding: 50px;
                background-color: #C80815;
	            border-radius: 4px;
                margin-left: auto;
                margin-right: auto;
	            text-align: center;
            }
            .text {
                font-weight: normal;
                font-size: medium;
                text-align: center;
                margin-bottom: 30px;
            }
            .button {
                width: 100%;
                background-color: #053C5E;
                color: #F1E4E8;
                padding: 10px 0 10px 0;
                border-radius: 4px;
                transition: all 0.3s linear;
                -webkit-transition: all 0.3s linear;
                -moz-transition: all 0.3s linear;
                text-align: center;
                cursor: pointer;
                display: block;
                text-decoration: none;
            }
            .button:hover {
                color: #F1E4E8;
                background-color: #042C3E;
                text-decoration: none;
            }
            .lead {
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <img src="/img/logo/Scriptory-icon-256.png" width="128px" height="128px">
            <h1 class="lead">Not like that!</h1>
            <p class="text">Scriptory requires JavaScript to work.<br>Please enable JavaScript.</p>
            <a class="button" href="/">Reload</a>
        </div>
    </body>
</html>