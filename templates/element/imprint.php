<?php
/**
 * @var \App\View\AppView $this
 */

use Cake\Core\Configure;
?>

<p>
    <strong><?= Configure::read('Imprint.name') ?></strong><br>
    <?= Configure::read('Imprint.email') ?>
	<?= Configure::read('Imprint.site') ?>
</p>

<?= Configure::read('Imprint.content') ?>
