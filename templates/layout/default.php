<?php

use Cake\Core\Configure;

$debug = Configure::read('debug');

$options = [
    'appBaseUrl' => '',
    'apiBaseUrl' => $this->Url->build('/api'),
    'appTitle' => Configure::read('appName') . ' - ' . Configure::read('appTitle'),
    'authorizationHeader' => Configure::read('authorizationHeader'),
];
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <?= $this->Html->charset() ?>
	<noscript>
		<style>html { display:none; }</style>
		<meta http-equiv="refresh" content="0;url=/pages/nojs">
	</noscript>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<title><?= Configure::read('appName') . ' - ' . Configure::read('appTitle') ?></title>
	<meta name="description" content="Find, create and share useful JavaScript scripts. Use them to create your own multifunctional boards.">
	<link rel="apple-touch-icon" sizes="180x180" href="/img/icons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/img/icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/img/icons/favicon-16x16.png">
	<link rel="manifest" href="/img/icons/site.webmanifest">
	<link rel="mask-icon" href="/img/icons/safari-pinned-tab.svg" color="#c80815">
	<link rel="shortcut icon" href="/img/icons/favicon.ico">
	<meta name="msapplication-TileColor" content="#c80815">
	<meta name="msapplication-config" content="/img/icons/browserconfig.xml">
	<meta name="theme-color" content="#c80815">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <?= $this->Html->meta('icon') ?>
    <?= $this->fetch('meta') ?>

    <?= $this->Html->css(json_decode(file_get_contents(WWW_ROOT . 'dist/manifest.json'), true)['main.css']) ?>
    <?= $this->fetch('css') ?>

</head>
<body>

<div id="vueApp"></div>
<?= $this->fetch('content') ?>

<script>
    var appConfig = <?= json_encode($options) ?>;
</script>

<?= $this->Html->script('/js/openpgpjs/dist/openpgp.min.js') ?>
<?= $this->Html->script(json_decode(file_get_contents(WWW_ROOT . 'dist/manifest.json'), true)['main.js']) ?>

<script>
    // Unfocus input when touching another element
    function removeFocus(evt) {
        if (evt.target !== document.activeElement) {
            document.activeElement.blur();
        }
    }
    document.body.addEventListener("touchstart", removeFocus);
</script>

</body>
</html>
