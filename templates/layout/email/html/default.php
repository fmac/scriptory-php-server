<!DOCTYPE html>
<html>
<head>
    <title><?= $this->fetch('title') ?></title>

	<style>
		#body {
			background-color: #321619 !important;
			color: #F7F4F3;
			font-family: 'Arial';
			padding: 20px;
			text-align: center;
		}
		a {
			color: #F7F4F3;
			text-decoration: none;
		}
		.mail-container {
			height: auto;
			width: 350px;
			padding: 0 50px 20px 50px;
			background-color: #053C5E;
			border: thin solid #F7F4F3;
			border-radius: 4px;
			margin: 20px auto 20px auto;
		}
		.mail-logo {
			height: 128px;
			width: 128px;
		}
		.mail-text {
			font-weight: normal;
			font-size: medium;
			text-align: center;
			margin-bottom: 30px;
		}
		.mail-button {
			width: 100%;
			background-color: transparent;
			color: #F7F4F3;
			margin: 20px auto 40px auto;
			padding: 10px 0 10px 0;
			border: thin solid #F7F4F3;
			border-radius: 4px;
			transition: all 0.3s linear;
			-webkit-transition: all 0.3s linear;
			-moz-transition: all 0.3s linear;
			cursor: pointer;
			display: block;
		}
		.mail-button:hover {
			background-color: #F7F4F3;
			color: #121619;
		}
	</style>
</head>
<body id="body">
    <?= $this->fetch('content') ?>
</body>
</html>
