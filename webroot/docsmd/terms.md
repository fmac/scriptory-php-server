# Terms and Conditions ("Terms")

Last updated: December 28, 2018

Please read these Terms and Conditions ("Terms", "Terms and Conditions") carefully before using the https://scriptory.io website (the "Service") operated by Francesco Camuffo (“we”, “us” or “our”).  
Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.  
By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.

## Access to the Service

Children under the age of 13 are not allowed to create an account or otherwise use the Service. Additionally, if you are in the European Economic Area, you must be over the age required by the laws of your country to create an account or otherwise use the Service, or we need to have received verifiable consent from your parent or legal guardian.  
If you are accepting these Terms on behalf of another legal entity, including a business or a government, you represent that you have full legal authority to bind such entity to these terms.

## Accounts

To use certain features of our Service, you may be required to create a scriptory.io account (an "Account") and provide us with a username, and certain other information about yourself as set forth in the Privacy Policy.  
You are solely responsible for the information associated with Your Account and anything that happens related to Your Account. You must maintain the security of your Account and promptly notify Scriptory if you discover or suspect that someone has accessed your Account without your permission. We recommend that you use a strong password that is used only with the Service.  
You will not license, sell, or transfer your Account without our prior written approval.

## User Content

The Service may contain information, text, links, graphics, photos, videos, or other materials (“Content”), including Content created with or submitted to the Service by you or through your Account (“Your Content”). We take no responsibility for and we do not expressly or implicitly endorse any of Your Content.

By submitting Your Content to the Service, you represent and warrant that you have all rights, power, and authority necessary to grant the rights to Your Content contained within these Terms. Because you alone are responsible for Your Content, you may expose yourself to liability if you post or share Content without all necessary rights.

You retain any ownership rights you have in Your Content, but you grant Scriptory the following license to use that Content:

When Your Content is created with or submitted to the Service, you grant us a worldwide, royalty-free, perpetual, irrevocable, non-exclusive, transferable, and sublicensable license to use, copy, modify, adapt, prepare derivative works from, distribute, perform, and display Your Content and any name, username, voice, or likeness provided in connection with Your Content in all media formats and channels now known or later developed. This license includes the right for us to make Your Content available for syndication, broadcast, distribution, or publication by other companies, organizations, or individuals who partner with Scriptory. You also agree that we may remove metadata associated with Your Content, and you irrevocably waive any claims and assertions of moral rights or attribution with respect to Your Content.

Any ideas, suggestions, and feedback about our Service that you provide to us are entirely voluntary, and you agree that Scriptory may use such ideas, suggestions, and feedback without compensation or obligation to you.

Although we have no obligation to screen, edit, or monitor Your Content, we may, in our sole discretion, delete or remove Your Content at any time and for any reason, including for a violation of these Terms, a violation of our Content Policy, or if you otherwise create liability for us.

## Third-Party Content, Advertisements and Promotions

The Service may contain links to third-party websites, products, or services, which may be posted by advertisers, our affiliates, our partners, or other users (“Third-Party Content”). Third-Party Content is not under our control, and we are not responsible for any of their websites, products, or services. Your use of Third-Party Content is at your own risk and you should make any investigation you feel necessary before proceeding with any transaction in connection with such Third-Party Content.

The Service may also contain sponsored Third-Party Content or advertisements. The type, degree, and targeting of advertisements are subject to change, and you acknowledge and agree that we may place advertisements in connection with the display of any Content or information on the Service, including Your Content.

If you choose to use the Service to conduct a promotion, including a contest or sweepstakes, you alone are responsible for conducting the promotion in compliance with all applicable laws and regulations. The terms of your promotion must specifically state that the promotion is not sponsored by, endorsed by, or associated with Scriptory and the rules for your promotion must require each entrant or participant to release Scriptory from any liability related to the promotion.

We strongly advise you to read the terms and conditions and privacy policies of any third-party web sites or services that you visit.

## Content Policy

### Prohibited content

Content is prohibited if it

* Is illegal
* Is pornography
* Is sexual or suggestive content involving minors
* Encourages or incites violence
* Threatens, harasses, or bullies or encourages others to do so
* Is personal and confidential information
* Impersonates someone in a misleading or deceptive manner
* Is spam

### Prohibited behavior

Prohibited behaviors include

* Doing anything that interferes with normal use of the Service
* Creating multiple accounts to evade punishment or avoid restrictions
* Use the Service to violate applicable law or infringe any person or entity's intellectual property or any other proprietary rights
* Attempt to gain unauthorized access to another user’s Account or to the Service (or to other computer systems or networks connected to or used together with the Service)
* Upload, transmit, or distribute to or through the Service any computer viruses, worms, or other software intended to interfere with the intended operation of a computer system or data
* Use the Service to harvest, collect, gather or assemble information or data regarding the Services or users of the Services except as permitted in these Terms
* Use the Service in any manner that could interfere with, disrupt, negatively affect, or inhibit other users from fully enjoying the Services or that could damage, disable, overburden, or impair the functioning of the Service in any manner
* Intentionally negate any user's actions to delete or edit their Content on the Service; or
* Access, query, or search the Service with any automated system, other than through our published interfaces and pursuant to their applicable terms. However, we conditionally grant permission to crawl the Service for the sole purpose of and solely to the extent necessary for creating publicly available searchable indices of the materials subject to the parameters set forth in our robots.txt file

## Termination

We may terminate or suspend access to our Service immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.

All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.

We may terminate or suspend your account immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.

Upon termination, your right to use the Service will immediately cease. If you wish to terminate your account, you may simply discontinue using the Service.

All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.

## Indemnity

Except to the extent prohibited by law, you agree to defend, indemnify, and hold us and our third party service providers harmless, including costs and attorneys’ fees, from any claim or demand made by any third party due to or arising out of (a) your use of the Service, (b) your violation of these Terms, (c) your violation of applicable laws or regulations, or (d) Your Content. We reserve the right to control the defense of any matter for which you are required to indemnify us, and you agree to cooperate with our defense of these claims.

## Disclaimers

THE SERVICE IS PROVIDED "AS IS" AND "AS AVAILABLE" WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. SCRIPTORY, ITS LICENSORS, AND ITS THIRD PARTY SERVICE PROVIDERS DO NOT WARRANT THAT THE SERVICE IS ACCURATE, COMPLETE, RELIABLE, CURRENT, OR ERROR FREE. SCRIPTORY DOES NOT CONTROL, ENDORSE, OR TAKE RESPONSIBILITY FOR ANY CONTENT AVAILABLE ON OR LINKED TO THE SERVICE OR THE ACTIONS OF ANY THIRD PARTY OR USER. WHILE SCRIPTORY ATTEMPTS TO MAKE YOUR ACCESS TO AND USE OF OUR SERVICE SAFE, WE DO NOT REPRESENT OR WARRANT THAT OUR SERVICE OR SERVERS ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS.

## Limitation of Liability

IN NO EVENT AND UNDER NO THEORY OF LIABILITY, INCLUDING CONTRACT, TORT, NEGLIGENCE, STRICT LIABILITY, WARRANTY, OR OTHERWISE, WILL SCRIPTORY BE LIABLE TO YOU FOR ANY INDIRECT, CONSEQUENTIAL, EXEMPLARY, INCIDENTAL, SPECIAL, OR PUNITIVE DAMAGES, OR LOST PROFITS ARISING FROM OR RELATING TO THESE TERMS OR THE SERVICE, INCLUDING THOSE ARISING FROM OR RELATING TO CONTENT MADE AVAILABLE ON THE SERVICE THAT IS ALLEGED TO BE DEFAMATORY, OFFENSIVE, OR ILLEGAL. ACCESS TO, AND USE OF, THE SERVICE IS AT YOUR OWN DISCRETION AND RISK, AND YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR DEVICE OR COMPUTER SYSTEM, OR LOSS OF DATA RESULTING THEREFROM. IN NO EVENT WILL THE AGGREGATE LIABILITY OF SCRIPTORY EXCEED THE GREATER OF ONE HUNDRED U.S. DOLLARS ($100) OR ANY AMOUNT YOU PAID SCRIPTORY IN THE PREVIOUS SIX MONTHS FOR THE SERVICES GIVING RISE TO THE CLAIM. THE LIMITATIONS OF THIS SECTION WILL APPLY TO ANY THEORY OF LIABILITY, INCLUDING THOSE BASED ON WARRANTY, CONTRACT, STATUTE, TORT (INCLUDING NEGLIGENCE) OR OTHERWISE, AND EVEN IF SCRIPTORY HAS BEEN ADVISED OF THE POSSIBILITY OF ANY SUCH DAMAGE, AND EVEN IF ANY REMEDY SET FORTH HEREIN IS FOUND TO HAVE FAILED ITS ESSENTIAL PURPOSE. THE FOREGOING LIMITATION OF LIABILITY WILL APPLY TO THE FULLEST EXTENT PERMITTED BY LAW IN THE APPLICABLE JURISDICTION.

## Governing Law

These Terms shall be governed and construed in accordance with the laws of Italy, without regard to its conflict of law provisions.

Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have between us regarding the Service.

## Changes

We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 30 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.

By continuing to access or use our Service after those revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, please stop using the Service.

## Contact Us

If you have any questions about these Terms, please contact us at this page [https://www.scriptory.io/docs/support](/docs/support).
