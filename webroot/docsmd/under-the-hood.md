# Under the hood

---

## Encryption

During the registration process an `ed25519` keypair is generated. These keys will be used to encrypt and decrypt your widgets data. And eventually to protect and sign data sent to other users.

Your private key is encrypted with `AES-256`. To obtain the passphrase, your password is joined to a user-specific salt and bcrypt-hashed, then, the result is joined to a keypair-specific salt and hashed again.

#### What is sent to the server

- Username and email.
- The user-specific salt.
- The SRP verifier.
- The keypair along with its private key passphrase salt.

---

## Registration

The authentication process is based on the [SRP] protocol so your password never leaves your computer. 

---

## Login

When you submit the login form the user salt is retrieved and the SRP dance starts. If the authentication is successful, the server emits an access token.
Then the browser can retrieve your keypair, and decrypt the private key (the next paragraph is important).

The beauty of end-to-end encryption is that nobody else than you and whoever you want can (if there are no bugs) read your data.  
This requires a key to be always available when you need to encrypt or decrypt something.  
Scriptory implements two strategies for storing locally your precious key passphrase.  
The next choice is up to you.

When you log in you must choose between `Remind me: Session` or `Remind me: Local`.
- Session (The pretty safe option) (Thanks to [ProtonMail] for this great idea)  
  This option stores your passphrase "split" in two parts, one part is stored in `window.name`, the other in `sessionStorage`. Both parts are computed to obtain the original passphrase when needed.  
  This kind of safety has a price. Every time you close the Scriptory window your key gets destroyed and you must login again (a good password keychain can make this less boring).
- Local (The less safe option)  
  This option stores your passphrase in the `localStorage` so it is preserved until you manually logout. The problem is that the passphrase is now among your browser data and could (and probably will) go out of your control, for example during a backup. Full disk encryption is advised.

---

## Scripts data

### What is encrypted

- Your widgets data is protected using your `ed25519` keypair. An example of such data is the Note widget's text.  
- Your storage providers (Dropbox) access tokens.

### What is not encrypted

The data needed to the site to work well, including scripts infos, scripts themselves (HTML, JS, whatever…) even if they are private, boards names, descriptions and layouts.


[SRP]: https://en.wikipedia.org/wiki/Secure_Remote_Password_protocol
[ProtonMail]: https://github.com/ProtonMail/WebClient
