<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    $routes->prefix('api', function (RouteBuilder $routes) {
        $routes->setExtensions(['json']);

        $routes->scope('/auth', ['controller' => 'Users'], function (RouteBuilder $routes) {
            $routes->connect('/signupInfo', ['action' => 'signupInfo', '_method' => 'GET']);
        });

        $routes->scope('/users', ['controller' => 'Users'], function (RouteBuilder $routes) {
            // Commons
            $routes->connect('/username-available', ['action' => 'usernameAvailable', '_method' => 'POST']);
            $routes->connect('/email-available', ['action' => 'emailAvailable', '_method' => 'POST']);
            $routes->connect('/signup', ['action' => 'signup', '_method' => 'POST']);
            $routes->connect('/verify-email', ['action' => 'verifyEmail', '_method' => 'POST']);
            $routes->connect('/lost-password', ['action' => 'lostPassword', '_method' => 'POST']);
            $routes->connect('/reset-password-info', ['action' => 'resetPasswordInfo', '_method' => 'POST']);
            $routes->connect('/reset-password', ['action' => 'resetPassword', '_method' => 'POST']);
            $routes->connect('/login-info', ['action' => 'loginInfo', '_method' => 'POST']);
            $routes->connect('/auth', ['action' => 'auth', '_method' => 'POST']);
            $routes->connect('/logout', ['action' => 'logout', '_method' => 'POST']);
            $routes->connect('/info', ['action' => 'info', '_method' => 'GET']);
            $routes->connect('/keypairs', ['action' => 'keypairs', '_method' => 'POST']);
            $routes->connect('/change-email', ['action' => 'changeEmail', '_method' => 'POST']);
            $routes->connect('/verify-change-email', ['action' => 'verifyChangeEmail', '_method' => 'POST']);
            $routes->connect('/change-password', ['action' => 'changePassword', '_method' => 'POST']);
            $routes->connect('/update-secrets', ['action' => 'updateSecrets', '_method' => 'POST']);
            $routes->connect('/update-config', ['action' => 'updateConfig', '_method' => 'POST']);
            // Resources
            $routes->connect('/profile', ['action' => 'profile', '_method' => 'POST']);
            $routes->connect('/boards', ['action' => 'boards', '_method' => 'POST']);
            $routes->connect('/scripts', ['action' => 'scripts', '_method' => 'POST']);
        });

        $routes->scope('/boards', ['controller' => 'Boards'], function (RouteBuilder $routes) {
            $routes->connect('/get-reference', ['action' => 'getReference', '_method' => 'POST']);
            $routes->connect('/user-list', ['action' => 'userList', '_method' => 'POST']);
            $routes->connect('/widgets-list', ['action' => 'widgetsList', '_method' => 'POST']);
            $routes->connect('/create', ['action' => 'create', '_method' => 'GET']);
            $routes->connect('/delete', ['action' => 'delete', '_method' => 'POST']);
            $routes->connect('/save-edit', ['action' => 'saveEdit', '_method' => 'POST']);
            $routes->connect('/get-widget-store', ['action' => 'getWidgetStore', '_method' => 'POST']);
            $routes->connect('/save-widget-store', ['action' => 'saveWidgetStore', '_method' => 'POST']);
        });

        $routes->scope('/scripts', ['controller' => 'Scripts'], function (RouteBuilder $routes) {
            $routes->connect('/get-script-reference', ['action' => 'getReference', '_method' => 'POST']);
            $routes->connect('/get-edit', ['action' => 'getEdit', '_method' => 'POST']);
            $routes->connect('/save-edit', ['action' => 'saveEdit', '_method' => 'POST']);
            $routes->connect('/user-list', ['action' => 'userList', '_method' => 'POST']);
            $routes->connect('/delete-script', ['action' => 'deleteScript', '_method' => 'POST']);
            $routes->connect('/clone', ['action' => 'clone', '_method' => 'POST']);
            $routes->connect('/get-script-as-widget', ['action' => 'getScriptAsWidget', '_method' => 'POST']);
            $routes->connect('/get-html', ['action' => 'getHtml', '_method' => 'POST', '_ext' => 'html']);
            $routes->connect('/new-and-popular', ['action' => 'newAndPopular', '_method' => 'GET']);
        });

        $routes->scope('/search', ['controller' => 'Search'], function (RouteBuilder $routes) {
            $routes->connect('/', ['action' => 'search', '_method' => 'POST']);
        });

        $routes->scope('/docs', ['controller' => 'Docs'], function (RouteBuilder $routes) {
            $routes->connect('/:name', ['action' => 'fetch', '_method' => 'GET'])
                ->setPass(['name']);
        });

        $routes->connect('/*', ['controller' => 'ApiApp', 'action' => 'notFound']);
    });

    $routes->scope('/', function (RouteBuilder $routes) {
        // Register scoped middleware for in scopes.
        /*$routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
            'httpOnly' => true
        ]));*/

        /**
         * Apply a middleware to the current route scope.
         * Requires middleware to be registered via `Application::routes()` with `registerMiddleware()`
         */
        // $routes->applyMiddleware('csrf');

        /**
         * Here, we are connecting '/' (base path) to a controller called 'Pages',
         * its action called 'display', and we pass a param to select the view file
         * to use (in this case, src/Template/Pages/home.ctp)...
         */
        //$routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);

        /**
         * ...and connect the rest of 'Pages' controller's URLs.
         */
        //$routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

        /**
         * Connect catchall routes for all controllers.
         *
         * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
         *
         * ```
         * $routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);
         * $routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);
         * ```
         *
         * Any route class can be used with this method, such as:
         * - DashedRoute
         * - InflectedRoute
         * - Route
         * - Or your own route class
         *
         * You can remove these routes once you've connected the
         * routes you want in your application.
         */
        //$routes->fallbacks(DashedRoute::class);

        /**
         * Any path render home
         */

        $routes->connect('/', ['controller' => 'Home', 'action' => 'index']);
        $routes->connect('/pages/nojs', ['controller' => 'Pages', 'action' => 'display', 'nojs']);
        $routes->connect('/sitemap-scripts.xml', ['controller' => 'Pages', 'action' => 'sitemapScripts', '_method' => 'GET', '_ext' => 'xml']);
        $routes->connect('/*', ['controller' => 'Home', 'action' => 'index']);
    });
});

/**
 * If you need a different set of middleware or none at all,
 * open new scope and define routes there.
 *
 * ```
 * Router::scope('/api', function (RouteBuilder $routes) {
 *     // No $routes->applyMiddleware() here.
 *     // Connect API actions here.
 * });
 * ```
 */
